﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class TrackableHandler : DefaultTrackableEventHandler
{
  protected override void OnTrackingFound()
  {
    base.OnTrackingFound();
    TrophyCollector coleccion = new TrophyCollector();
    Transform trophy = gameObject.transform.GetChild(0);
    switch (trophy.name)
    {
      case "Models":
        foreach (Transform child in trophy)
        {
          coleccion.Collect(child.name);
        }
        break;
      default:
        coleccion.Collect(trophy.name);
        break;
    }
  }
  
}
