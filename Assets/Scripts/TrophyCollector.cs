﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TrophyCollector : MonoBehaviour
{
	public static List<string> trofeos = new List<string>();

	void Start()
	{
		trofeos.Clear();
		if (PlayerPrefs.GetString("trofeos") == "")
		{
			//No hacer nada
		}
		else
		{	
			foreach (string objeto in PlayerPrefs.GetString("trofeos").Split(','))
			{
				trofeos.Add(objeto);
			}
		}
	}

	public void Collect(string name)
	{
		if (trofeos.Contains(name))
		{
			//No hacer nada
		}
		else
		{
            trofeos.Add(name);
            string[] trophyArray = trofeos.ToArray();
            PlayerPrefs.SetString ("trofeos", string.Join(",", trophyArray));
		}
	}
}