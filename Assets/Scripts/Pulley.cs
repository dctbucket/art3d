﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pulley : MonoBehaviour {
	public GameObject polea;
	public GameObject target;

	public GameObject result;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		Quaternion rotationDelta = Quaternion.FromToRotation(target.transform.forward, polea.transform.forward);
		float rotation = rotationDelta.eulerAngles.y;
		Debug.Log(rotation);
	
		
	}
}
