﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuNavigator : MonoBehaviour
{
	public GameObject activateView;
	public GameObject deactivateView;

	public void Navigate()
	{
		activateView.SetActive(true);
		deactivateView.SetActive(false);
	}
}
