﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hyperlinks : MonoBehaviour
{
  public void Politicas()
  {
    Application.OpenURL("https://tec.mx/es/politicas-de-privacidad-del-tecnologico-de-monterrey");
  }

  public void Legal()
  {
    Application.OpenURL("https://tec.mx/es/aviso-legal");
  }

  public void Privacidad()
  {
    Application.OpenURL("https://tec.mx/es/avisos-de-privacidad");
  }

  public void Targets()
  {
    Application.OpenURL("https://thce911.github.io/ART3D");
  }
}
