﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TrophySceneNavigator : MonoBehaviour
{
	public void changeModelLeft()
	{
		int indice = TrophySceneManager.trofeos.IndexOf(TrophySceneManager.ActiveTrophy);
		TrophySceneManager.ActiveTrophy.SetActive(false);
		if (indice == 0)
		{
			var lastModel = TrophySceneManager.trofeos.Count-1;
			TrophySceneManager.trofeos[lastModel].SetActive(true);
			TrophySceneManager.ActiveTrophy = TrophySceneManager.trofeos[lastModel];
			GameObject
			.Find("MainView__ModelNameLegend")
			.GetComponent<Text>()
			.text = TrophySceneManager.trofeos[lastModel].name;
		}
		else
		{
			TrophySceneManager.trofeos[indice-1].SetActive(true);
			TrophySceneManager.ActiveTrophy = TrophySceneManager.trofeos[indice-1];
			GameObject
			.Find("MainView__ModelNameLegend")
			.GetComponent<Text>()
			.text = TrophySceneManager.trofeos[indice-1].name;
		}
	}

	public void changeModelRight()
    {
        int indice = TrophySceneManager.trofeos.IndexOf(TrophySceneManager.ActiveTrophy);
        TrophySceneManager.ActiveTrophy.SetActive(false);
        if (indice+1 == TrophySceneManager.trofeos.Count)
        {
            TrophySceneManager.trofeos[0].SetActive(true);
            TrophySceneManager.ActiveTrophy = TrophySceneManager.trofeos[0];
            GameObject
			.Find("MainView__ModelNameLegend")
			.GetComponent<Text>()
			.text = TrophySceneManager.ActiveTrophy.name;
        }
        else
        {
            TrophySceneManager.trofeos[indice+1].SetActive(true);
            TrophySceneManager.ActiveTrophy = TrophySceneManager.trofeos[indice+1];
            GameObject
			.Find("MainView__ModelNameLegend")
			.GetComponent<Text>()
			.text = TrophySceneManager.ActiveTrophy.name;
        }
    }
}
