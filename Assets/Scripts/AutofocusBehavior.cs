﻿using UnityEngine;
using System.Collections;
using System;
using Vuforia;

public class AutofocusBehavior : MonoBehaviour 
{
	void Start() 
	{
		var vuforia = VuforiaARController.Instance;
		vuforia.RegisterVuforiaStartedCallback(OnVuforiaStarted);
		vuforia.RegisterOnPauseCallback(OnPaused);
	}

	void OnVuforiaStarted()
	{
		CameraDevice
		.Instance
		.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
	}

	void OnPaused(bool paused)
	{
		if (!paused) // Reanudación
		{
			// Activar autofocus una vez que se reanuda el app
			CameraDevice
			.Instance
			.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
		}
	}
}