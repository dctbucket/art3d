﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TrophySceneManager : MonoBehaviour
{
	public static List<GameObject> trofeos = new List<GameObject>();
	public static GameObject ActiveTrophy;

	void Start()
	{
		foreach(Transform child in transform)
		{
			child.gameObject.SetActive(false);
			if (TrophyCollector.trofeos.Contains(child.gameObject.name))
			{
				trofeos.Add(child.gameObject);
			}
		}
		if(TrophyCollector.trofeos.Count == 0)
		{
			GameObject.Find("MainView__NavigateRight").SetActive(false);
			GameObject.Find("MainView__NavigateLeft").SetActive(false);
		}
		else
		{
			if(TrophyCollector.trofeos.Count == 1)
			{
				GameObject.Find("MainView__NavigateRight").SetActive(false);
				GameObject.Find("MainView__NavigateLeft").SetActive(false);
			}
			GameObject.Find("MainView__NoTrophiesLegend").SetActive(false);
			trofeos[0].SetActive(true);
			GameObject.Find("MainView__ModelNameLegend").GetComponent<Text>().text = trofeos[0].name;
			ActiveTrophy = trofeos[0];
		}
	}

	void OnDestroy()
	{
		trofeos.Clear();
	}
}
