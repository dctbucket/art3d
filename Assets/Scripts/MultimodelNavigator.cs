﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MultimodelNavigator : MonoBehaviour
{
	private List<GameObject> modelos = new List<GameObject>();
	private GameObject activeTrophy;
    public Button LeftButton, RightButton;

	void Start()
    {
        foreach (Transform child in transform)
        {
			modelos.Add(child.gameObject);
		}
        modelos[0].SetActive(true);
	    activeTrophy = modelos[0];
        LeftButton.onClick.AddListener(Left);
        RightButton.onClick.AddListener(Right);
	}

	private void Left()
	{
        activeTrophy.SetActive(false);
        int indice = modelos.IndexOf(activeTrophy);
        if (indice == 0)
        {
            modelos[modelos.Count-1].SetActive(true);
            activeTrophy = modelos[modelos.Count-1];
        }
        else
        {
            modelos[indice-1].SetActive(true);
            activeTrophy = modelos[indice-1];
        }
    }

	private void Right()
	{
        activeTrophy.SetActive(false);
        int indice = modelos.IndexOf(activeTrophy);
        if (indice + 1 == modelos.Count)
        {
            modelos[0].SetActive(true);
            activeTrophy = modelos[0];
        }
        else
        {
            modelos[indice+1].SetActive(true);
            activeTrophy = modelos[indice+1];
        }
    }
    
    void OnDestroy()
    {
        modelos.Clear();
    }	
}
