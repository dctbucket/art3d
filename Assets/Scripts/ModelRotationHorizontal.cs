﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModelRotationHorizontal : MonoBehaviour
{
	private Quaternion rotateBy; //objeto quaternion representa rotaciones
	[Range(0.1f, 1.0f)]
	public float sensitivity;

	void Update()
	{
		if (Input.touchCount == 1)
		{
			for (int i = 0; i < Input.touchCount; ++i)
			{
				if (Input.GetTouch (i).phase == TouchPhase.Moved)
				{
					Vector3 touchPos = Input.GetTouch(i).deltaPosition;
					Rotate(-touchPos.x, -touchPos.y);
					transform.localRotation *= rotateBy;
				}
			}
		}
	}

	void Rotate(float rotateLeftRight, float rotateUpDown)
	{
		Camera cam = Camera.main;
		Vector3 relativeUp = cam.transform.TransformDirection(Vector3.up);
		Vector3 relativeRight = cam.transform.TransformDirection(Vector3.right);
		Vector3 objectRelativeUp = transform.InverseTransformDirection(relativeUp);
		Vector3 objectRelaviveRight = transform.InverseTransformDirection(relativeRight);

		float angleX = rotateLeftRight/transform.localScale.x * sensitivity;
		float angleY = -rotateUpDown/transform.localScale.x * sensitivity;

		rotateBy = Quaternion.AngleAxis(angleX, objectRelativeUp);

		
	}
}