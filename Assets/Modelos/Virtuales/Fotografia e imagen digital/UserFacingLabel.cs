﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserFacingLabel : MonoBehaviour {

  public Transform Camera;
  public Vector3 Rotation;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
    transform.LookAt(Camera);
    transform.Rotate(Rotation);
	}
}
