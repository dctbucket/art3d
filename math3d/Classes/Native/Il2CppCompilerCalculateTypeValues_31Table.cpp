﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Lean.Touch.LeanFinger
struct LeanFinger_t14B8CB80FF55DDB97259D004E0F1149542C099BD;
// Lean.Touch.LeanFingerTrail/LeanFingerEvent
struct LeanFingerEvent_tD6458AB7B064E1069692CF3A1289471CB83CF0E0;
// Lean.Touch.LeanFingerUp/LeanFingerEvent
struct LeanFingerEvent_tFF0C9F5C453F71CD3D71C86992C9F95902FD37E3;
// Lean.Touch.LeanMultiTap/IntEvent
struct IntEvent_tBDB887CFEC7CBB3D58FC47454658620CE252DD88;
// Lean.Touch.LeanSelectable
struct LeanSelectable_t2376D6100D5CC4EEC7F3BB4A3B0D57810CBDA1D9;
// Lean.Touch.LeanSelectable/LeanFingerEvent
struct LeanFingerEvent_t1F6EB317C16105591BB549C360BDBE8E67911B5D;
// System.Action`1<Lean.Touch.LeanFinger>
struct Action_1_t955501676F4CE8E1E3A8F8000378BDBB68BCD2EC;
// System.Action`1<System.Collections.Generic.List`1<Lean.Touch.LeanFinger>>
struct Action_1_t506E130BAFC89D90111C6710E76354559705EEC2;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.List`1<Lean.Touch.LeanFinger>
struct List_1_tDA85F67BB9390A7D9940CB6D1B08D2CE04BCE37E;
// System.Collections.Generic.List`1<Lean.Touch.LeanFingerTrail/Link>
struct List_1_t63D1A0902FCFE927E419131F4C94ED32434F2A6B;
// System.Collections.Generic.List`1<Lean.Touch.LeanSelectable>
struct List_1_tC8A62EF7B84DC935549C656900E96FD2D73BCEFD;
// System.Collections.Generic.List`1<Lean.Touch.LeanSnapshot>
struct List_1_t6A0CEEA00D532975F81F5FD612BBA2033E2CD75A;
// System.Collections.Generic.List`1<Lean.Touch.LeanTouch>
struct List_1_t740D263DA628BB692D9D7C9A9E75369CB3FEA371;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_tB291263EEE72B9F137CA4DC19F039DE672D08028;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.String
struct String_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.Camera
struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F;
// UnityEngine.LineRenderer
struct LineRenderer_tD225C480F28F28A4D737866474F21001B803B7C3;
// UnityEngine.Rigidbody
struct Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.UI.Text
struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef LINK_T683F8BEE786C9CD7E97AC02899AFC24621551AD5_H
#define LINK_T683F8BEE786C9CD7E97AC02899AFC24621551AD5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lean.Touch.LeanFingerTrail_Link
struct  Link_t683F8BEE786C9CD7E97AC02899AFC24621551AD5  : public RuntimeObject
{
public:
	// Lean.Touch.LeanFinger Lean.Touch.LeanFingerTrail_Link::Finger
	LeanFinger_t14B8CB80FF55DDB97259D004E0F1149542C099BD * ___Finger_0;
	// UnityEngine.LineRenderer Lean.Touch.LeanFingerTrail_Link::Line
	LineRenderer_tD225C480F28F28A4D737866474F21001B803B7C3 * ___Line_1;

public:
	inline static int32_t get_offset_of_Finger_0() { return static_cast<int32_t>(offsetof(Link_t683F8BEE786C9CD7E97AC02899AFC24621551AD5, ___Finger_0)); }
	inline LeanFinger_t14B8CB80FF55DDB97259D004E0F1149542C099BD * get_Finger_0() const { return ___Finger_0; }
	inline LeanFinger_t14B8CB80FF55DDB97259D004E0F1149542C099BD ** get_address_of_Finger_0() { return &___Finger_0; }
	inline void set_Finger_0(LeanFinger_t14B8CB80FF55DDB97259D004E0F1149542C099BD * value)
	{
		___Finger_0 = value;
		Il2CppCodeGenWriteBarrier((&___Finger_0), value);
	}

	inline static int32_t get_offset_of_Line_1() { return static_cast<int32_t>(offsetof(Link_t683F8BEE786C9CD7E97AC02899AFC24621551AD5, ___Line_1)); }
	inline LineRenderer_tD225C480F28F28A4D737866474F21001B803B7C3 * get_Line_1() const { return ___Line_1; }
	inline LineRenderer_tD225C480F28F28A4D737866474F21001B803B7C3 ** get_address_of_Line_1() { return &___Line_1; }
	inline void set_Line_1(LineRenderer_tD225C480F28F28A4D737866474F21001B803B7C3 * value)
	{
		___Line_1 = value;
		Il2CppCodeGenWriteBarrier((&___Line_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINK_T683F8BEE786C9CD7E97AC02899AFC24621551AD5_H
#ifndef LEANGESTURE_T836FA998DDAC09DAD15355F59083CCC443A51FD0_H
#define LEANGESTURE_T836FA998DDAC09DAD15355F59083CCC443A51FD0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lean.Touch.LeanGesture
struct  LeanGesture_t836FA998DDAC09DAD15355F59083CCC443A51FD0  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEANGESTURE_T836FA998DDAC09DAD15355F59083CCC443A51FD0_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#define UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * ___m_PersistentCalls_1;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_2;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_Calls_0)); }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_CallsDirty_2)); }
	inline bool get_m_CallsDirty_2() const { return ___m_CallsDirty_2; }
	inline bool* get_address_of_m_CallsDirty_2() { return &___m_CallsDirty_2; }
	inline void set_m_CallsDirty_2(bool value)
	{
		___m_CallsDirty_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef UNITYEVENT_1_T554DEF90DE5F39CFBA46645C3351533BA9B4E137_H
#define UNITYEVENT_1_T554DEF90DE5F39CFBA46645C3351533BA9B4E137_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<Lean.Touch.LeanFinger>
struct  UnityEvent_1_t554DEF90DE5F39CFBA46645C3351533BA9B4E137  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_t554DEF90DE5F39CFBA46645C3351533BA9B4E137, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T554DEF90DE5F39CFBA46645C3351533BA9B4E137_H
#ifndef UNITYEVENT_2_T72075D01C70D2F1F80A5D4E03FECFAFA7BAAAFA8_H
#define UNITYEVENT_2_T72075D01C70D2F1F80A5D4E03FECFAFA7BAAAFA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`2<System.Int32,System.Int32>
struct  UnityEvent_2_t72075D01C70D2F1F80A5D4E03FECFAFA7BAAAFA8  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`2::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_2_t72075D01C70D2F1F80A5D4E03FECFAFA7BAAAFA8, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_2_T72075D01C70D2F1F80A5D4E03FECFAFA7BAAAFA8_H
#ifndef LAYERMASK_TBB9173D8B6939D476E67E849280AC9F4EC4D93B0_H
#define LAYERMASK_TBB9173D8B6939D476E67E849280AC9F4EC4D93B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_TBB9173D8B6939D476E67E849280AC9F4EC4D93B0_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef LEANFINGER_T14B8CB80FF55DDB97259D004E0F1149542C099BD_H
#define LEANFINGER_T14B8CB80FF55DDB97259D004E0F1149542C099BD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lean.Touch.LeanFinger
struct  LeanFinger_t14B8CB80FF55DDB97259D004E0F1149542C099BD  : public RuntimeObject
{
public:
	// System.Int32 Lean.Touch.LeanFinger::Index
	int32_t ___Index_0;
	// System.Single Lean.Touch.LeanFinger::Age
	float ___Age_1;
	// System.Boolean Lean.Touch.LeanFinger::Set
	bool ___Set_2;
	// System.Boolean Lean.Touch.LeanFinger::LastSet
	bool ___LastSet_3;
	// System.Boolean Lean.Touch.LeanFinger::Tap
	bool ___Tap_4;
	// System.Int32 Lean.Touch.LeanFinger::TapCount
	int32_t ___TapCount_5;
	// System.Boolean Lean.Touch.LeanFinger::Swipe
	bool ___Swipe_6;
	// UnityEngine.Vector2 Lean.Touch.LeanFinger::StartScreenPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___StartScreenPosition_7;
	// UnityEngine.Vector2 Lean.Touch.LeanFinger::LastScreenPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___LastScreenPosition_8;
	// UnityEngine.Vector2 Lean.Touch.LeanFinger::ScreenPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___ScreenPosition_9;
	// System.Boolean Lean.Touch.LeanFinger::StartedOverGui
	bool ___StartedOverGui_10;
	// System.Collections.Generic.List`1<Lean.Touch.LeanSnapshot> Lean.Touch.LeanFinger::Snapshots
	List_1_t6A0CEEA00D532975F81F5FD612BBA2033E2CD75A * ___Snapshots_11;

public:
	inline static int32_t get_offset_of_Index_0() { return static_cast<int32_t>(offsetof(LeanFinger_t14B8CB80FF55DDB97259D004E0F1149542C099BD, ___Index_0)); }
	inline int32_t get_Index_0() const { return ___Index_0; }
	inline int32_t* get_address_of_Index_0() { return &___Index_0; }
	inline void set_Index_0(int32_t value)
	{
		___Index_0 = value;
	}

	inline static int32_t get_offset_of_Age_1() { return static_cast<int32_t>(offsetof(LeanFinger_t14B8CB80FF55DDB97259D004E0F1149542C099BD, ___Age_1)); }
	inline float get_Age_1() const { return ___Age_1; }
	inline float* get_address_of_Age_1() { return &___Age_1; }
	inline void set_Age_1(float value)
	{
		___Age_1 = value;
	}

	inline static int32_t get_offset_of_Set_2() { return static_cast<int32_t>(offsetof(LeanFinger_t14B8CB80FF55DDB97259D004E0F1149542C099BD, ___Set_2)); }
	inline bool get_Set_2() const { return ___Set_2; }
	inline bool* get_address_of_Set_2() { return &___Set_2; }
	inline void set_Set_2(bool value)
	{
		___Set_2 = value;
	}

	inline static int32_t get_offset_of_LastSet_3() { return static_cast<int32_t>(offsetof(LeanFinger_t14B8CB80FF55DDB97259D004E0F1149542C099BD, ___LastSet_3)); }
	inline bool get_LastSet_3() const { return ___LastSet_3; }
	inline bool* get_address_of_LastSet_3() { return &___LastSet_3; }
	inline void set_LastSet_3(bool value)
	{
		___LastSet_3 = value;
	}

	inline static int32_t get_offset_of_Tap_4() { return static_cast<int32_t>(offsetof(LeanFinger_t14B8CB80FF55DDB97259D004E0F1149542C099BD, ___Tap_4)); }
	inline bool get_Tap_4() const { return ___Tap_4; }
	inline bool* get_address_of_Tap_4() { return &___Tap_4; }
	inline void set_Tap_4(bool value)
	{
		___Tap_4 = value;
	}

	inline static int32_t get_offset_of_TapCount_5() { return static_cast<int32_t>(offsetof(LeanFinger_t14B8CB80FF55DDB97259D004E0F1149542C099BD, ___TapCount_5)); }
	inline int32_t get_TapCount_5() const { return ___TapCount_5; }
	inline int32_t* get_address_of_TapCount_5() { return &___TapCount_5; }
	inline void set_TapCount_5(int32_t value)
	{
		___TapCount_5 = value;
	}

	inline static int32_t get_offset_of_Swipe_6() { return static_cast<int32_t>(offsetof(LeanFinger_t14B8CB80FF55DDB97259D004E0F1149542C099BD, ___Swipe_6)); }
	inline bool get_Swipe_6() const { return ___Swipe_6; }
	inline bool* get_address_of_Swipe_6() { return &___Swipe_6; }
	inline void set_Swipe_6(bool value)
	{
		___Swipe_6 = value;
	}

	inline static int32_t get_offset_of_StartScreenPosition_7() { return static_cast<int32_t>(offsetof(LeanFinger_t14B8CB80FF55DDB97259D004E0F1149542C099BD, ___StartScreenPosition_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_StartScreenPosition_7() const { return ___StartScreenPosition_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_StartScreenPosition_7() { return &___StartScreenPosition_7; }
	inline void set_StartScreenPosition_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___StartScreenPosition_7 = value;
	}

	inline static int32_t get_offset_of_LastScreenPosition_8() { return static_cast<int32_t>(offsetof(LeanFinger_t14B8CB80FF55DDB97259D004E0F1149542C099BD, ___LastScreenPosition_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_LastScreenPosition_8() const { return ___LastScreenPosition_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_LastScreenPosition_8() { return &___LastScreenPosition_8; }
	inline void set_LastScreenPosition_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___LastScreenPosition_8 = value;
	}

	inline static int32_t get_offset_of_ScreenPosition_9() { return static_cast<int32_t>(offsetof(LeanFinger_t14B8CB80FF55DDB97259D004E0F1149542C099BD, ___ScreenPosition_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_ScreenPosition_9() const { return ___ScreenPosition_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_ScreenPosition_9() { return &___ScreenPosition_9; }
	inline void set_ScreenPosition_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___ScreenPosition_9 = value;
	}

	inline static int32_t get_offset_of_StartedOverGui_10() { return static_cast<int32_t>(offsetof(LeanFinger_t14B8CB80FF55DDB97259D004E0F1149542C099BD, ___StartedOverGui_10)); }
	inline bool get_StartedOverGui_10() const { return ___StartedOverGui_10; }
	inline bool* get_address_of_StartedOverGui_10() { return &___StartedOverGui_10; }
	inline void set_StartedOverGui_10(bool value)
	{
		___StartedOverGui_10 = value;
	}

	inline static int32_t get_offset_of_Snapshots_11() { return static_cast<int32_t>(offsetof(LeanFinger_t14B8CB80FF55DDB97259D004E0F1149542C099BD, ___Snapshots_11)); }
	inline List_1_t6A0CEEA00D532975F81F5FD612BBA2033E2CD75A * get_Snapshots_11() const { return ___Snapshots_11; }
	inline List_1_t6A0CEEA00D532975F81F5FD612BBA2033E2CD75A ** get_address_of_Snapshots_11() { return &___Snapshots_11; }
	inline void set_Snapshots_11(List_1_t6A0CEEA00D532975F81F5FD612BBA2033E2CD75A * value)
	{
		___Snapshots_11 = value;
		Il2CppCodeGenWriteBarrier((&___Snapshots_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEANFINGER_T14B8CB80FF55DDB97259D004E0F1149542C099BD_H
#ifndef LEANFINGEREVENT_T1F14AA8CA6D25026BA19E57A021BFE1F51D5FF34_H
#define LEANFINGEREVENT_T1F14AA8CA6D25026BA19E57A021BFE1F51D5FF34_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lean.Touch.LeanFingerTap_LeanFingerEvent
struct  LeanFingerEvent_t1F14AA8CA6D25026BA19E57A021BFE1F51D5FF34  : public UnityEvent_1_t554DEF90DE5F39CFBA46645C3351533BA9B4E137
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEANFINGEREVENT_T1F14AA8CA6D25026BA19E57A021BFE1F51D5FF34_H
#ifndef LEANFINGEREVENT_TD6458AB7B064E1069692CF3A1289471CB83CF0E0_H
#define LEANFINGEREVENT_TD6458AB7B064E1069692CF3A1289471CB83CF0E0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lean.Touch.LeanFingerTrail_LeanFingerEvent
struct  LeanFingerEvent_tD6458AB7B064E1069692CF3A1289471CB83CF0E0  : public UnityEvent_1_t554DEF90DE5F39CFBA46645C3351533BA9B4E137
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEANFINGEREVENT_TD6458AB7B064E1069692CF3A1289471CB83CF0E0_H
#ifndef LEANFINGEREVENT_TFF0C9F5C453F71CD3D71C86992C9F95902FD37E3_H
#define LEANFINGEREVENT_TFF0C9F5C453F71CD3D71C86992C9F95902FD37E3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lean.Touch.LeanFingerUp_LeanFingerEvent
struct  LeanFingerEvent_tFF0C9F5C453F71CD3D71C86992C9F95902FD37E3  : public UnityEvent_1_t554DEF90DE5F39CFBA46645C3351533BA9B4E137
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEANFINGEREVENT_TFF0C9F5C453F71CD3D71C86992C9F95902FD37E3_H
#ifndef INTEVENT_TBDB887CFEC7CBB3D58FC47454658620CE252DD88_H
#define INTEVENT_TBDB887CFEC7CBB3D58FC47454658620CE252DD88_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lean.Touch.LeanMultiTap_IntEvent
struct  IntEvent_tBDB887CFEC7CBB3D58FC47454658620CE252DD88  : public UnityEvent_2_t72075D01C70D2F1F80A5D4E03FECFAFA7BAAAFA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTEVENT_TBDB887CFEC7CBB3D58FC47454658620CE252DD88_H
#ifndef SEARCHTYPE_TE639CAB9A46520EB025A32DAC345D3525AA07CB3_H
#define SEARCHTYPE_TE639CAB9A46520EB025A32DAC345D3525AA07CB3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lean.Touch.LeanPressSelect_SearchType
struct  SearchType_tE639CAB9A46520EB025A32DAC345D3525AA07CB3 
{
public:
	// System.Int32 Lean.Touch.LeanPressSelect_SearchType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SearchType_tE639CAB9A46520EB025A32DAC345D3525AA07CB3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEARCHTYPE_TE639CAB9A46520EB025A32DAC345D3525AA07CB3_H
#ifndef SELECTTYPE_TE2E0E9D7ABE838F6EE237CDCE9DC8943ED9C8321_H
#define SELECTTYPE_TE2E0E9D7ABE838F6EE237CDCE9DC8943ED9C8321_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lean.Touch.LeanPressSelect_SelectType
struct  SelectType_tE2E0E9D7ABE838F6EE237CDCE9DC8943ED9C8321 
{
public:
	// System.Int32 Lean.Touch.LeanPressSelect_SelectType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SelectType_tE2E0E9D7ABE838F6EE237CDCE9DC8943ED9C8321, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTTYPE_TE2E0E9D7ABE838F6EE237CDCE9DC8943ED9C8321_H
#ifndef RESELECTTYPE_T773521332897EB0BC6CDB76604DF8C798F115567_H
#define RESELECTTYPE_T773521332897EB0BC6CDB76604DF8C798F115567_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lean.Touch.LeanSelect_ReselectType
struct  ReselectType_t773521332897EB0BC6CDB76604DF8C798F115567 
{
public:
	// System.Int32 Lean.Touch.LeanSelect_ReselectType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ReselectType_t773521332897EB0BC6CDB76604DF8C798F115567, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESELECTTYPE_T773521332897EB0BC6CDB76604DF8C798F115567_H
#ifndef SEARCHTYPE_TCE9BA229D09A933AB8089F0F2CFF2136FA05BD8D_H
#define SEARCHTYPE_TCE9BA229D09A933AB8089F0F2CFF2136FA05BD8D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lean.Touch.LeanSelect_SearchType
struct  SearchType_tCE9BA229D09A933AB8089F0F2CFF2136FA05BD8D 
{
public:
	// System.Int32 Lean.Touch.LeanSelect_SearchType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SearchType_tCE9BA229D09A933AB8089F0F2CFF2136FA05BD8D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEARCHTYPE_TCE9BA229D09A933AB8089F0F2CFF2136FA05BD8D_H
#ifndef SELECTTYPE_T77B376C78F823909EF5808C38F09392243A899C7_H
#define SELECTTYPE_T77B376C78F823909EF5808C38F09392243A899C7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lean.Touch.LeanSelect_SelectType
struct  SelectType_t77B376C78F823909EF5808C38F09392243A899C7 
{
public:
	// System.Int32 Lean.Touch.LeanSelect_SelectType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SelectType_t77B376C78F823909EF5808C38F09392243A899C7, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTTYPE_T77B376C78F823909EF5808C38F09392243A899C7_H
#ifndef LEANFINGEREVENT_T1F6EB317C16105591BB549C360BDBE8E67911B5D_H
#define LEANFINGEREVENT_T1F6EB317C16105591BB549C360BDBE8E67911B5D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lean.Touch.LeanSelectable_LeanFingerEvent
struct  LeanFingerEvent_t1F6EB317C16105591BB549C360BDBE8E67911B5D  : public UnityEvent_1_t554DEF90DE5F39CFBA46645C3351533BA9B4E137
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEANFINGEREVENT_T1F6EB317C16105591BB549C360BDBE8E67911B5D_H
#ifndef LEANSNAPSHOT_T345B3981EAE95E412F6DEBEE4C2A8EA07385AA7F_H
#define LEANSNAPSHOT_T345B3981EAE95E412F6DEBEE4C2A8EA07385AA7F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lean.Touch.LeanSnapshot
struct  LeanSnapshot_t345B3981EAE95E412F6DEBEE4C2A8EA07385AA7F  : public RuntimeObject
{
public:
	// System.Single Lean.Touch.LeanSnapshot::Age
	float ___Age_0;
	// UnityEngine.Vector2 Lean.Touch.LeanSnapshot::ScreenPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___ScreenPosition_1;

public:
	inline static int32_t get_offset_of_Age_0() { return static_cast<int32_t>(offsetof(LeanSnapshot_t345B3981EAE95E412F6DEBEE4C2A8EA07385AA7F, ___Age_0)); }
	inline float get_Age_0() const { return ___Age_0; }
	inline float* get_address_of_Age_0() { return &___Age_0; }
	inline void set_Age_0(float value)
	{
		___Age_0 = value;
	}

	inline static int32_t get_offset_of_ScreenPosition_1() { return static_cast<int32_t>(offsetof(LeanSnapshot_t345B3981EAE95E412F6DEBEE4C2A8EA07385AA7F, ___ScreenPosition_1)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_ScreenPosition_1() const { return ___ScreenPosition_1; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_ScreenPosition_1() { return &___ScreenPosition_1; }
	inline void set_ScreenPosition_1(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___ScreenPosition_1 = value;
	}
};

struct LeanSnapshot_t345B3981EAE95E412F6DEBEE4C2A8EA07385AA7F_StaticFields
{
public:
	// System.Collections.Generic.List`1<Lean.Touch.LeanSnapshot> Lean.Touch.LeanSnapshot::InactiveSnapshots
	List_1_t6A0CEEA00D532975F81F5FD612BBA2033E2CD75A * ___InactiveSnapshots_2;

public:
	inline static int32_t get_offset_of_InactiveSnapshots_2() { return static_cast<int32_t>(offsetof(LeanSnapshot_t345B3981EAE95E412F6DEBEE4C2A8EA07385AA7F_StaticFields, ___InactiveSnapshots_2)); }
	inline List_1_t6A0CEEA00D532975F81F5FD612BBA2033E2CD75A * get_InactiveSnapshots_2() const { return ___InactiveSnapshots_2; }
	inline List_1_t6A0CEEA00D532975F81F5FD612BBA2033E2CD75A ** get_address_of_InactiveSnapshots_2() { return &___InactiveSnapshots_2; }
	inline void set_InactiveSnapshots_2(List_1_t6A0CEEA00D532975F81F5FD612BBA2033E2CD75A * value)
	{
		___InactiveSnapshots_2 = value;
		Il2CppCodeGenWriteBarrier((&___InactiveSnapshots_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEANSNAPSHOT_T345B3981EAE95E412F6DEBEE4C2A8EA07385AA7F_H
#ifndef KEYCODE_TC93EA87C5A6901160B583ADFCD3EF6726570DC3C_H
#define KEYCODE_TC93EA87C5A6901160B583ADFCD3EF6726570DC3C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.KeyCode
struct  KeyCode_tC93EA87C5A6901160B583ADFCD3EF6726570DC3C 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(KeyCode_tC93EA87C5A6901160B583ADFCD3EF6726570DC3C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCODE_TC93EA87C5A6901160B583ADFCD3EF6726570DC3C_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef LEANFINGERTRAIL_T24936637155A64FC6CF0738216FFDFEF10C90D98_H
#define LEANFINGERTRAIL_T24936637155A64FC6CF0738216FFDFEF10C90D98_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lean.Touch.LeanFingerTrail
struct  LeanFingerTrail_t24936637155A64FC6CF0738216FFDFEF10C90D98  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean Lean.Touch.LeanFingerTrail::IgnoreGuiFingers
	bool ___IgnoreGuiFingers_4;
	// UnityEngine.LineRenderer Lean.Touch.LeanFingerTrail::Prefab
	LineRenderer_tD225C480F28F28A4D737866474F21001B803B7C3 * ___Prefab_5;
	// System.Single Lean.Touch.LeanFingerTrail::Distance
	float ___Distance_6;
	// System.Int32 Lean.Touch.LeanFingerTrail::MaxLines
	int32_t ___MaxLines_7;
	// Lean.Touch.LeanFingerTrail_LeanFingerEvent Lean.Touch.LeanFingerTrail::OnFingerUp
	LeanFingerEvent_tD6458AB7B064E1069692CF3A1289471CB83CF0E0 * ___OnFingerUp_8;
	// System.Collections.Generic.List`1<Lean.Touch.LeanFingerTrail_Link> Lean.Touch.LeanFingerTrail::links
	List_1_t63D1A0902FCFE927E419131F4C94ED32434F2A6B * ___links_9;

public:
	inline static int32_t get_offset_of_IgnoreGuiFingers_4() { return static_cast<int32_t>(offsetof(LeanFingerTrail_t24936637155A64FC6CF0738216FFDFEF10C90D98, ___IgnoreGuiFingers_4)); }
	inline bool get_IgnoreGuiFingers_4() const { return ___IgnoreGuiFingers_4; }
	inline bool* get_address_of_IgnoreGuiFingers_4() { return &___IgnoreGuiFingers_4; }
	inline void set_IgnoreGuiFingers_4(bool value)
	{
		___IgnoreGuiFingers_4 = value;
	}

	inline static int32_t get_offset_of_Prefab_5() { return static_cast<int32_t>(offsetof(LeanFingerTrail_t24936637155A64FC6CF0738216FFDFEF10C90D98, ___Prefab_5)); }
	inline LineRenderer_tD225C480F28F28A4D737866474F21001B803B7C3 * get_Prefab_5() const { return ___Prefab_5; }
	inline LineRenderer_tD225C480F28F28A4D737866474F21001B803B7C3 ** get_address_of_Prefab_5() { return &___Prefab_5; }
	inline void set_Prefab_5(LineRenderer_tD225C480F28F28A4D737866474F21001B803B7C3 * value)
	{
		___Prefab_5 = value;
		Il2CppCodeGenWriteBarrier((&___Prefab_5), value);
	}

	inline static int32_t get_offset_of_Distance_6() { return static_cast<int32_t>(offsetof(LeanFingerTrail_t24936637155A64FC6CF0738216FFDFEF10C90D98, ___Distance_6)); }
	inline float get_Distance_6() const { return ___Distance_6; }
	inline float* get_address_of_Distance_6() { return &___Distance_6; }
	inline void set_Distance_6(float value)
	{
		___Distance_6 = value;
	}

	inline static int32_t get_offset_of_MaxLines_7() { return static_cast<int32_t>(offsetof(LeanFingerTrail_t24936637155A64FC6CF0738216FFDFEF10C90D98, ___MaxLines_7)); }
	inline int32_t get_MaxLines_7() const { return ___MaxLines_7; }
	inline int32_t* get_address_of_MaxLines_7() { return &___MaxLines_7; }
	inline void set_MaxLines_7(int32_t value)
	{
		___MaxLines_7 = value;
	}

	inline static int32_t get_offset_of_OnFingerUp_8() { return static_cast<int32_t>(offsetof(LeanFingerTrail_t24936637155A64FC6CF0738216FFDFEF10C90D98, ___OnFingerUp_8)); }
	inline LeanFingerEvent_tD6458AB7B064E1069692CF3A1289471CB83CF0E0 * get_OnFingerUp_8() const { return ___OnFingerUp_8; }
	inline LeanFingerEvent_tD6458AB7B064E1069692CF3A1289471CB83CF0E0 ** get_address_of_OnFingerUp_8() { return &___OnFingerUp_8; }
	inline void set_OnFingerUp_8(LeanFingerEvent_tD6458AB7B064E1069692CF3A1289471CB83CF0E0 * value)
	{
		___OnFingerUp_8 = value;
		Il2CppCodeGenWriteBarrier((&___OnFingerUp_8), value);
	}

	inline static int32_t get_offset_of_links_9() { return static_cast<int32_t>(offsetof(LeanFingerTrail_t24936637155A64FC6CF0738216FFDFEF10C90D98, ___links_9)); }
	inline List_1_t63D1A0902FCFE927E419131F4C94ED32434F2A6B * get_links_9() const { return ___links_9; }
	inline List_1_t63D1A0902FCFE927E419131F4C94ED32434F2A6B ** get_address_of_links_9() { return &___links_9; }
	inline void set_links_9(List_1_t63D1A0902FCFE927E419131F4C94ED32434F2A6B * value)
	{
		___links_9 = value;
		Il2CppCodeGenWriteBarrier((&___links_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEANFINGERTRAIL_T24936637155A64FC6CF0738216FFDFEF10C90D98_H
#ifndef LEANFINGERUP_T97C1EB0B82492855A1854C258410DAADF00222A3_H
#define LEANFINGERUP_T97C1EB0B82492855A1854C258410DAADF00222A3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lean.Touch.LeanFingerUp
struct  LeanFingerUp_t97C1EB0B82492855A1854C258410DAADF00222A3  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean Lean.Touch.LeanFingerUp::IgnoreIfOverGui
	bool ___IgnoreIfOverGui_4;
	// System.Boolean Lean.Touch.LeanFingerUp::IgnoreIfStartedOverGui
	bool ___IgnoreIfStartedOverGui_5;
	// Lean.Touch.LeanFingerUp_LeanFingerEvent Lean.Touch.LeanFingerUp::OnFingerUp
	LeanFingerEvent_tFF0C9F5C453F71CD3D71C86992C9F95902FD37E3 * ___OnFingerUp_6;

public:
	inline static int32_t get_offset_of_IgnoreIfOverGui_4() { return static_cast<int32_t>(offsetof(LeanFingerUp_t97C1EB0B82492855A1854C258410DAADF00222A3, ___IgnoreIfOverGui_4)); }
	inline bool get_IgnoreIfOverGui_4() const { return ___IgnoreIfOverGui_4; }
	inline bool* get_address_of_IgnoreIfOverGui_4() { return &___IgnoreIfOverGui_4; }
	inline void set_IgnoreIfOverGui_4(bool value)
	{
		___IgnoreIfOverGui_4 = value;
	}

	inline static int32_t get_offset_of_IgnoreIfStartedOverGui_5() { return static_cast<int32_t>(offsetof(LeanFingerUp_t97C1EB0B82492855A1854C258410DAADF00222A3, ___IgnoreIfStartedOverGui_5)); }
	inline bool get_IgnoreIfStartedOverGui_5() const { return ___IgnoreIfStartedOverGui_5; }
	inline bool* get_address_of_IgnoreIfStartedOverGui_5() { return &___IgnoreIfStartedOverGui_5; }
	inline void set_IgnoreIfStartedOverGui_5(bool value)
	{
		___IgnoreIfStartedOverGui_5 = value;
	}

	inline static int32_t get_offset_of_OnFingerUp_6() { return static_cast<int32_t>(offsetof(LeanFingerUp_t97C1EB0B82492855A1854C258410DAADF00222A3, ___OnFingerUp_6)); }
	inline LeanFingerEvent_tFF0C9F5C453F71CD3D71C86992C9F95902FD37E3 * get_OnFingerUp_6() const { return ___OnFingerUp_6; }
	inline LeanFingerEvent_tFF0C9F5C453F71CD3D71C86992C9F95902FD37E3 ** get_address_of_OnFingerUp_6() { return &___OnFingerUp_6; }
	inline void set_OnFingerUp_6(LeanFingerEvent_tFF0C9F5C453F71CD3D71C86992C9F95902FD37E3 * value)
	{
		___OnFingerUp_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnFingerUp_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEANFINGERUP_T97C1EB0B82492855A1854C258410DAADF00222A3_H
#ifndef LEANMULTITAP_T02652AE95847C5613DB2B447D5B0386B5DD943B7_H
#define LEANMULTITAP_T02652AE95847C5613DB2B447D5B0386B5DD943B7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lean.Touch.LeanMultiTap
struct  LeanMultiTap_t02652AE95847C5613DB2B447D5B0386B5DD943B7  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean Lean.Touch.LeanMultiTap::IgnoreGuiFingers
	bool ___IgnoreGuiFingers_4;
	// System.Boolean Lean.Touch.LeanMultiTap::MultiTap
	bool ___MultiTap_5;
	// System.Int32 Lean.Touch.LeanMultiTap::MultiTapCount
	int32_t ___MultiTapCount_6;
	// System.Int32 Lean.Touch.LeanMultiTap::HighestFingerCount
	int32_t ___HighestFingerCount_7;
	// Lean.Touch.LeanMultiTap_IntEvent Lean.Touch.LeanMultiTap::OnMultiTap
	IntEvent_tBDB887CFEC7CBB3D58FC47454658620CE252DD88 * ___OnMultiTap_8;
	// System.Single Lean.Touch.LeanMultiTap::age
	float ___age_9;
	// System.Int32 Lean.Touch.LeanMultiTap::lastFingerCount
	int32_t ___lastFingerCount_10;

public:
	inline static int32_t get_offset_of_IgnoreGuiFingers_4() { return static_cast<int32_t>(offsetof(LeanMultiTap_t02652AE95847C5613DB2B447D5B0386B5DD943B7, ___IgnoreGuiFingers_4)); }
	inline bool get_IgnoreGuiFingers_4() const { return ___IgnoreGuiFingers_4; }
	inline bool* get_address_of_IgnoreGuiFingers_4() { return &___IgnoreGuiFingers_4; }
	inline void set_IgnoreGuiFingers_4(bool value)
	{
		___IgnoreGuiFingers_4 = value;
	}

	inline static int32_t get_offset_of_MultiTap_5() { return static_cast<int32_t>(offsetof(LeanMultiTap_t02652AE95847C5613DB2B447D5B0386B5DD943B7, ___MultiTap_5)); }
	inline bool get_MultiTap_5() const { return ___MultiTap_5; }
	inline bool* get_address_of_MultiTap_5() { return &___MultiTap_5; }
	inline void set_MultiTap_5(bool value)
	{
		___MultiTap_5 = value;
	}

	inline static int32_t get_offset_of_MultiTapCount_6() { return static_cast<int32_t>(offsetof(LeanMultiTap_t02652AE95847C5613DB2B447D5B0386B5DD943B7, ___MultiTapCount_6)); }
	inline int32_t get_MultiTapCount_6() const { return ___MultiTapCount_6; }
	inline int32_t* get_address_of_MultiTapCount_6() { return &___MultiTapCount_6; }
	inline void set_MultiTapCount_6(int32_t value)
	{
		___MultiTapCount_6 = value;
	}

	inline static int32_t get_offset_of_HighestFingerCount_7() { return static_cast<int32_t>(offsetof(LeanMultiTap_t02652AE95847C5613DB2B447D5B0386B5DD943B7, ___HighestFingerCount_7)); }
	inline int32_t get_HighestFingerCount_7() const { return ___HighestFingerCount_7; }
	inline int32_t* get_address_of_HighestFingerCount_7() { return &___HighestFingerCount_7; }
	inline void set_HighestFingerCount_7(int32_t value)
	{
		___HighestFingerCount_7 = value;
	}

	inline static int32_t get_offset_of_OnMultiTap_8() { return static_cast<int32_t>(offsetof(LeanMultiTap_t02652AE95847C5613DB2B447D5B0386B5DD943B7, ___OnMultiTap_8)); }
	inline IntEvent_tBDB887CFEC7CBB3D58FC47454658620CE252DD88 * get_OnMultiTap_8() const { return ___OnMultiTap_8; }
	inline IntEvent_tBDB887CFEC7CBB3D58FC47454658620CE252DD88 ** get_address_of_OnMultiTap_8() { return &___OnMultiTap_8; }
	inline void set_OnMultiTap_8(IntEvent_tBDB887CFEC7CBB3D58FC47454658620CE252DD88 * value)
	{
		___OnMultiTap_8 = value;
		Il2CppCodeGenWriteBarrier((&___OnMultiTap_8), value);
	}

	inline static int32_t get_offset_of_age_9() { return static_cast<int32_t>(offsetof(LeanMultiTap_t02652AE95847C5613DB2B447D5B0386B5DD943B7, ___age_9)); }
	inline float get_age_9() const { return ___age_9; }
	inline float* get_address_of_age_9() { return &___age_9; }
	inline void set_age_9(float value)
	{
		___age_9 = value;
	}

	inline static int32_t get_offset_of_lastFingerCount_10() { return static_cast<int32_t>(offsetof(LeanMultiTap_t02652AE95847C5613DB2B447D5B0386B5DD943B7, ___lastFingerCount_10)); }
	inline int32_t get_lastFingerCount_10() const { return ___lastFingerCount_10; }
	inline int32_t* get_address_of_lastFingerCount_10() { return &___lastFingerCount_10; }
	inline void set_lastFingerCount_10(int32_t value)
	{
		___lastFingerCount_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEANMULTITAP_T02652AE95847C5613DB2B447D5B0386B5DD943B7_H
#ifndef LEANMULTITAPINFO_T0A9005A97DB044CE4F2BE882CA91356582EC21B4_H
#define LEANMULTITAPINFO_T0A9005A97DB044CE4F2BE882CA91356582EC21B4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lean.Touch.LeanMultiTapInfo
struct  LeanMultiTapInfo_t0A9005A97DB044CE4F2BE882CA91356582EC21B4  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text Lean.Touch.LeanMultiTapInfo::Text
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___Text_4;

public:
	inline static int32_t get_offset_of_Text_4() { return static_cast<int32_t>(offsetof(LeanMultiTapInfo_t0A9005A97DB044CE4F2BE882CA91356582EC21B4, ___Text_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_Text_4() const { return ___Text_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_Text_4() { return &___Text_4; }
	inline void set_Text_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___Text_4 = value;
		Il2CppCodeGenWriteBarrier((&___Text_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEANMULTITAPINFO_T0A9005A97DB044CE4F2BE882CA91356582EC21B4_H
#ifndef LEANOPENURL_T9162996B214A907B7225A61EF4DCF2D4ACEDAE93_H
#define LEANOPENURL_T9162996B214A907B7225A61EF4DCF2D4ACEDAE93_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lean.Touch.LeanOpenUrl
struct  LeanOpenUrl_t9162996B214A907B7225A61EF4DCF2D4ACEDAE93  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEANOPENURL_T9162996B214A907B7225A61EF4DCF2D4ACEDAE93_H
#ifndef LEANPITCHYAW_T8626ADD8CAF55496C8F102476E5815B67482004E_H
#define LEANPITCHYAW_T8626ADD8CAF55496C8F102476E5815B67482004E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lean.Touch.LeanPitchYaw
struct  LeanPitchYaw_t8626ADD8CAF55496C8F102476E5815B67482004E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean Lean.Touch.LeanPitchYaw::IgnoreGuiFingers
	bool ___IgnoreGuiFingers_4;
	// System.Int32 Lean.Touch.LeanPitchYaw::RequiredFingerCount
	int32_t ___RequiredFingerCount_5;
	// UnityEngine.Camera Lean.Touch.LeanPitchYaw::Camera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___Camera_6;
	// System.Single Lean.Touch.LeanPitchYaw::Pitch
	float ___Pitch_7;
	// System.Single Lean.Touch.LeanPitchYaw::PitchSensitivity
	float ___PitchSensitivity_8;
	// System.Boolean Lean.Touch.LeanPitchYaw::PitchClamp
	bool ___PitchClamp_9;
	// System.Single Lean.Touch.LeanPitchYaw::PitchMin
	float ___PitchMin_10;
	// System.Single Lean.Touch.LeanPitchYaw::PitchMax
	float ___PitchMax_11;
	// System.Single Lean.Touch.LeanPitchYaw::Yaw
	float ___Yaw_12;
	// System.Single Lean.Touch.LeanPitchYaw::YawSensitivity
	float ___YawSensitivity_13;
	// System.Boolean Lean.Touch.LeanPitchYaw::YawClamp
	bool ___YawClamp_14;
	// System.Single Lean.Touch.LeanPitchYaw::YawMin
	float ___YawMin_15;
	// System.Single Lean.Touch.LeanPitchYaw::YawMax
	float ___YawMax_16;

public:
	inline static int32_t get_offset_of_IgnoreGuiFingers_4() { return static_cast<int32_t>(offsetof(LeanPitchYaw_t8626ADD8CAF55496C8F102476E5815B67482004E, ___IgnoreGuiFingers_4)); }
	inline bool get_IgnoreGuiFingers_4() const { return ___IgnoreGuiFingers_4; }
	inline bool* get_address_of_IgnoreGuiFingers_4() { return &___IgnoreGuiFingers_4; }
	inline void set_IgnoreGuiFingers_4(bool value)
	{
		___IgnoreGuiFingers_4 = value;
	}

	inline static int32_t get_offset_of_RequiredFingerCount_5() { return static_cast<int32_t>(offsetof(LeanPitchYaw_t8626ADD8CAF55496C8F102476E5815B67482004E, ___RequiredFingerCount_5)); }
	inline int32_t get_RequiredFingerCount_5() const { return ___RequiredFingerCount_5; }
	inline int32_t* get_address_of_RequiredFingerCount_5() { return &___RequiredFingerCount_5; }
	inline void set_RequiredFingerCount_5(int32_t value)
	{
		___RequiredFingerCount_5 = value;
	}

	inline static int32_t get_offset_of_Camera_6() { return static_cast<int32_t>(offsetof(LeanPitchYaw_t8626ADD8CAF55496C8F102476E5815B67482004E, ___Camera_6)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_Camera_6() const { return ___Camera_6; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_Camera_6() { return &___Camera_6; }
	inline void set_Camera_6(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___Camera_6 = value;
		Il2CppCodeGenWriteBarrier((&___Camera_6), value);
	}

	inline static int32_t get_offset_of_Pitch_7() { return static_cast<int32_t>(offsetof(LeanPitchYaw_t8626ADD8CAF55496C8F102476E5815B67482004E, ___Pitch_7)); }
	inline float get_Pitch_7() const { return ___Pitch_7; }
	inline float* get_address_of_Pitch_7() { return &___Pitch_7; }
	inline void set_Pitch_7(float value)
	{
		___Pitch_7 = value;
	}

	inline static int32_t get_offset_of_PitchSensitivity_8() { return static_cast<int32_t>(offsetof(LeanPitchYaw_t8626ADD8CAF55496C8F102476E5815B67482004E, ___PitchSensitivity_8)); }
	inline float get_PitchSensitivity_8() const { return ___PitchSensitivity_8; }
	inline float* get_address_of_PitchSensitivity_8() { return &___PitchSensitivity_8; }
	inline void set_PitchSensitivity_8(float value)
	{
		___PitchSensitivity_8 = value;
	}

	inline static int32_t get_offset_of_PitchClamp_9() { return static_cast<int32_t>(offsetof(LeanPitchYaw_t8626ADD8CAF55496C8F102476E5815B67482004E, ___PitchClamp_9)); }
	inline bool get_PitchClamp_9() const { return ___PitchClamp_9; }
	inline bool* get_address_of_PitchClamp_9() { return &___PitchClamp_9; }
	inline void set_PitchClamp_9(bool value)
	{
		___PitchClamp_9 = value;
	}

	inline static int32_t get_offset_of_PitchMin_10() { return static_cast<int32_t>(offsetof(LeanPitchYaw_t8626ADD8CAF55496C8F102476E5815B67482004E, ___PitchMin_10)); }
	inline float get_PitchMin_10() const { return ___PitchMin_10; }
	inline float* get_address_of_PitchMin_10() { return &___PitchMin_10; }
	inline void set_PitchMin_10(float value)
	{
		___PitchMin_10 = value;
	}

	inline static int32_t get_offset_of_PitchMax_11() { return static_cast<int32_t>(offsetof(LeanPitchYaw_t8626ADD8CAF55496C8F102476E5815B67482004E, ___PitchMax_11)); }
	inline float get_PitchMax_11() const { return ___PitchMax_11; }
	inline float* get_address_of_PitchMax_11() { return &___PitchMax_11; }
	inline void set_PitchMax_11(float value)
	{
		___PitchMax_11 = value;
	}

	inline static int32_t get_offset_of_Yaw_12() { return static_cast<int32_t>(offsetof(LeanPitchYaw_t8626ADD8CAF55496C8F102476E5815B67482004E, ___Yaw_12)); }
	inline float get_Yaw_12() const { return ___Yaw_12; }
	inline float* get_address_of_Yaw_12() { return &___Yaw_12; }
	inline void set_Yaw_12(float value)
	{
		___Yaw_12 = value;
	}

	inline static int32_t get_offset_of_YawSensitivity_13() { return static_cast<int32_t>(offsetof(LeanPitchYaw_t8626ADD8CAF55496C8F102476E5815B67482004E, ___YawSensitivity_13)); }
	inline float get_YawSensitivity_13() const { return ___YawSensitivity_13; }
	inline float* get_address_of_YawSensitivity_13() { return &___YawSensitivity_13; }
	inline void set_YawSensitivity_13(float value)
	{
		___YawSensitivity_13 = value;
	}

	inline static int32_t get_offset_of_YawClamp_14() { return static_cast<int32_t>(offsetof(LeanPitchYaw_t8626ADD8CAF55496C8F102476E5815B67482004E, ___YawClamp_14)); }
	inline bool get_YawClamp_14() const { return ___YawClamp_14; }
	inline bool* get_address_of_YawClamp_14() { return &___YawClamp_14; }
	inline void set_YawClamp_14(bool value)
	{
		___YawClamp_14 = value;
	}

	inline static int32_t get_offset_of_YawMin_15() { return static_cast<int32_t>(offsetof(LeanPitchYaw_t8626ADD8CAF55496C8F102476E5815B67482004E, ___YawMin_15)); }
	inline float get_YawMin_15() const { return ___YawMin_15; }
	inline float* get_address_of_YawMin_15() { return &___YawMin_15; }
	inline void set_YawMin_15(float value)
	{
		___YawMin_15 = value;
	}

	inline static int32_t get_offset_of_YawMax_16() { return static_cast<int32_t>(offsetof(LeanPitchYaw_t8626ADD8CAF55496C8F102476E5815B67482004E, ___YawMax_16)); }
	inline float get_YawMax_16() const { return ___YawMax_16; }
	inline float* get_address_of_YawMax_16() { return &___YawMax_16; }
	inline void set_YawMax_16(float value)
	{
		___YawMax_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEANPITCHYAW_T8626ADD8CAF55496C8F102476E5815B67482004E_H
#ifndef LEANPRESSSELECT_T1E686A5833E1CEF9D7E186D8C6B30417D2368F40_H
#define LEANPRESSSELECT_T1E686A5833E1CEF9D7E186D8C6B30417D2368F40_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lean.Touch.LeanPressSelect
struct  LeanPressSelect_t1E686A5833E1CEF9D7E186D8C6B30417D2368F40  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean Lean.Touch.LeanPressSelect::IgnoreGuiFingers
	bool ___IgnoreGuiFingers_4;
	// System.Boolean Lean.Touch.LeanPressSelect::DeselectOnExit
	bool ___DeselectOnExit_5;
	// Lean.Touch.LeanPressSelect_SelectType Lean.Touch.LeanPressSelect::SelectUsing
	int32_t ___SelectUsing_6;
	// UnityEngine.LayerMask Lean.Touch.LeanPressSelect::LayerMask
	LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  ___LayerMask_7;
	// Lean.Touch.LeanPressSelect_SearchType Lean.Touch.LeanPressSelect::Search
	int32_t ___Search_8;
	// System.Collections.Generic.List`1<Lean.Touch.LeanSelectable> Lean.Touch.LeanPressSelect::CurrentSelectables
	List_1_tC8A62EF7B84DC935549C656900E96FD2D73BCEFD * ___CurrentSelectables_9;

public:
	inline static int32_t get_offset_of_IgnoreGuiFingers_4() { return static_cast<int32_t>(offsetof(LeanPressSelect_t1E686A5833E1CEF9D7E186D8C6B30417D2368F40, ___IgnoreGuiFingers_4)); }
	inline bool get_IgnoreGuiFingers_4() const { return ___IgnoreGuiFingers_4; }
	inline bool* get_address_of_IgnoreGuiFingers_4() { return &___IgnoreGuiFingers_4; }
	inline void set_IgnoreGuiFingers_4(bool value)
	{
		___IgnoreGuiFingers_4 = value;
	}

	inline static int32_t get_offset_of_DeselectOnExit_5() { return static_cast<int32_t>(offsetof(LeanPressSelect_t1E686A5833E1CEF9D7E186D8C6B30417D2368F40, ___DeselectOnExit_5)); }
	inline bool get_DeselectOnExit_5() const { return ___DeselectOnExit_5; }
	inline bool* get_address_of_DeselectOnExit_5() { return &___DeselectOnExit_5; }
	inline void set_DeselectOnExit_5(bool value)
	{
		___DeselectOnExit_5 = value;
	}

	inline static int32_t get_offset_of_SelectUsing_6() { return static_cast<int32_t>(offsetof(LeanPressSelect_t1E686A5833E1CEF9D7E186D8C6B30417D2368F40, ___SelectUsing_6)); }
	inline int32_t get_SelectUsing_6() const { return ___SelectUsing_6; }
	inline int32_t* get_address_of_SelectUsing_6() { return &___SelectUsing_6; }
	inline void set_SelectUsing_6(int32_t value)
	{
		___SelectUsing_6 = value;
	}

	inline static int32_t get_offset_of_LayerMask_7() { return static_cast<int32_t>(offsetof(LeanPressSelect_t1E686A5833E1CEF9D7E186D8C6B30417D2368F40, ___LayerMask_7)); }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  get_LayerMask_7() const { return ___LayerMask_7; }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0 * get_address_of_LayerMask_7() { return &___LayerMask_7; }
	inline void set_LayerMask_7(LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  value)
	{
		___LayerMask_7 = value;
	}

	inline static int32_t get_offset_of_Search_8() { return static_cast<int32_t>(offsetof(LeanPressSelect_t1E686A5833E1CEF9D7E186D8C6B30417D2368F40, ___Search_8)); }
	inline int32_t get_Search_8() const { return ___Search_8; }
	inline int32_t* get_address_of_Search_8() { return &___Search_8; }
	inline void set_Search_8(int32_t value)
	{
		___Search_8 = value;
	}

	inline static int32_t get_offset_of_CurrentSelectables_9() { return static_cast<int32_t>(offsetof(LeanPressSelect_t1E686A5833E1CEF9D7E186D8C6B30417D2368F40, ___CurrentSelectables_9)); }
	inline List_1_tC8A62EF7B84DC935549C656900E96FD2D73BCEFD * get_CurrentSelectables_9() const { return ___CurrentSelectables_9; }
	inline List_1_tC8A62EF7B84DC935549C656900E96FD2D73BCEFD ** get_address_of_CurrentSelectables_9() { return &___CurrentSelectables_9; }
	inline void set_CurrentSelectables_9(List_1_tC8A62EF7B84DC935549C656900E96FD2D73BCEFD * value)
	{
		___CurrentSelectables_9 = value;
		Il2CppCodeGenWriteBarrier((&___CurrentSelectables_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEANPRESSSELECT_T1E686A5833E1CEF9D7E186D8C6B30417D2368F40_H
#ifndef LEANROTATE_TF9E60051B47B69E60B506E874877A6377012D1A5_H
#define LEANROTATE_TF9E60051B47B69E60B506E874877A6377012D1A5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lean.Touch.LeanRotate
struct  LeanRotate_tF9E60051B47B69E60B506E874877A6377012D1A5  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean Lean.Touch.LeanRotate::IgnoreGuiFingers
	bool ___IgnoreGuiFingers_4;
	// System.Int32 Lean.Touch.LeanRotate::RequiredFingerCount
	int32_t ___RequiredFingerCount_5;
	// Lean.Touch.LeanSelectable Lean.Touch.LeanRotate::RequiredSelectable
	LeanSelectable_t2376D6100D5CC4EEC7F3BB4A3B0D57810CBDA1D9 * ___RequiredSelectable_6;
	// UnityEngine.Camera Lean.Touch.LeanRotate::Camera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___Camera_7;
	// UnityEngine.Vector3 Lean.Touch.LeanRotate::RotateAxis
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___RotateAxis_8;
	// System.Boolean Lean.Touch.LeanRotate::Relative
	bool ___Relative_9;

public:
	inline static int32_t get_offset_of_IgnoreGuiFingers_4() { return static_cast<int32_t>(offsetof(LeanRotate_tF9E60051B47B69E60B506E874877A6377012D1A5, ___IgnoreGuiFingers_4)); }
	inline bool get_IgnoreGuiFingers_4() const { return ___IgnoreGuiFingers_4; }
	inline bool* get_address_of_IgnoreGuiFingers_4() { return &___IgnoreGuiFingers_4; }
	inline void set_IgnoreGuiFingers_4(bool value)
	{
		___IgnoreGuiFingers_4 = value;
	}

	inline static int32_t get_offset_of_RequiredFingerCount_5() { return static_cast<int32_t>(offsetof(LeanRotate_tF9E60051B47B69E60B506E874877A6377012D1A5, ___RequiredFingerCount_5)); }
	inline int32_t get_RequiredFingerCount_5() const { return ___RequiredFingerCount_5; }
	inline int32_t* get_address_of_RequiredFingerCount_5() { return &___RequiredFingerCount_5; }
	inline void set_RequiredFingerCount_5(int32_t value)
	{
		___RequiredFingerCount_5 = value;
	}

	inline static int32_t get_offset_of_RequiredSelectable_6() { return static_cast<int32_t>(offsetof(LeanRotate_tF9E60051B47B69E60B506E874877A6377012D1A5, ___RequiredSelectable_6)); }
	inline LeanSelectable_t2376D6100D5CC4EEC7F3BB4A3B0D57810CBDA1D9 * get_RequiredSelectable_6() const { return ___RequiredSelectable_6; }
	inline LeanSelectable_t2376D6100D5CC4EEC7F3BB4A3B0D57810CBDA1D9 ** get_address_of_RequiredSelectable_6() { return &___RequiredSelectable_6; }
	inline void set_RequiredSelectable_6(LeanSelectable_t2376D6100D5CC4EEC7F3BB4A3B0D57810CBDA1D9 * value)
	{
		___RequiredSelectable_6 = value;
		Il2CppCodeGenWriteBarrier((&___RequiredSelectable_6), value);
	}

	inline static int32_t get_offset_of_Camera_7() { return static_cast<int32_t>(offsetof(LeanRotate_tF9E60051B47B69E60B506E874877A6377012D1A5, ___Camera_7)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_Camera_7() const { return ___Camera_7; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_Camera_7() { return &___Camera_7; }
	inline void set_Camera_7(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___Camera_7 = value;
		Il2CppCodeGenWriteBarrier((&___Camera_7), value);
	}

	inline static int32_t get_offset_of_RotateAxis_8() { return static_cast<int32_t>(offsetof(LeanRotate_tF9E60051B47B69E60B506E874877A6377012D1A5, ___RotateAxis_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_RotateAxis_8() const { return ___RotateAxis_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_RotateAxis_8() { return &___RotateAxis_8; }
	inline void set_RotateAxis_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___RotateAxis_8 = value;
	}

	inline static int32_t get_offset_of_Relative_9() { return static_cast<int32_t>(offsetof(LeanRotate_tF9E60051B47B69E60B506E874877A6377012D1A5, ___Relative_9)); }
	inline bool get_Relative_9() const { return ___Relative_9; }
	inline bool* get_address_of_Relative_9() { return &___Relative_9; }
	inline void set_Relative_9(bool value)
	{
		___Relative_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEANROTATE_TF9E60051B47B69E60B506E874877A6377012D1A5_H
#ifndef LEANSCALE_TDB8973F254335EC3C2393B21E572A79BC89C5D7C_H
#define LEANSCALE_TDB8973F254335EC3C2393B21E572A79BC89C5D7C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lean.Touch.LeanScale
struct  LeanScale_tDB8973F254335EC3C2393B21E572A79BC89C5D7C  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean Lean.Touch.LeanScale::IgnoreGuiFingers
	bool ___IgnoreGuiFingers_4;
	// System.Int32 Lean.Touch.LeanScale::RequiredFingerCount
	int32_t ___RequiredFingerCount_5;
	// Lean.Touch.LeanSelectable Lean.Touch.LeanScale::RequiredSelectable
	LeanSelectable_t2376D6100D5CC4EEC7F3BB4A3B0D57810CBDA1D9 * ___RequiredSelectable_6;
	// System.Single Lean.Touch.LeanScale::WheelSensitivity
	float ___WheelSensitivity_7;
	// UnityEngine.Camera Lean.Touch.LeanScale::Camera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___Camera_8;
	// System.Boolean Lean.Touch.LeanScale::Relative
	bool ___Relative_9;
	// System.Boolean Lean.Touch.LeanScale::ScaleClamp
	bool ___ScaleClamp_10;
	// UnityEngine.Vector3 Lean.Touch.LeanScale::ScaleMin
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___ScaleMin_11;
	// UnityEngine.Vector3 Lean.Touch.LeanScale::ScaleMax
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___ScaleMax_12;

public:
	inline static int32_t get_offset_of_IgnoreGuiFingers_4() { return static_cast<int32_t>(offsetof(LeanScale_tDB8973F254335EC3C2393B21E572A79BC89C5D7C, ___IgnoreGuiFingers_4)); }
	inline bool get_IgnoreGuiFingers_4() const { return ___IgnoreGuiFingers_4; }
	inline bool* get_address_of_IgnoreGuiFingers_4() { return &___IgnoreGuiFingers_4; }
	inline void set_IgnoreGuiFingers_4(bool value)
	{
		___IgnoreGuiFingers_4 = value;
	}

	inline static int32_t get_offset_of_RequiredFingerCount_5() { return static_cast<int32_t>(offsetof(LeanScale_tDB8973F254335EC3C2393B21E572A79BC89C5D7C, ___RequiredFingerCount_5)); }
	inline int32_t get_RequiredFingerCount_5() const { return ___RequiredFingerCount_5; }
	inline int32_t* get_address_of_RequiredFingerCount_5() { return &___RequiredFingerCount_5; }
	inline void set_RequiredFingerCount_5(int32_t value)
	{
		___RequiredFingerCount_5 = value;
	}

	inline static int32_t get_offset_of_RequiredSelectable_6() { return static_cast<int32_t>(offsetof(LeanScale_tDB8973F254335EC3C2393B21E572A79BC89C5D7C, ___RequiredSelectable_6)); }
	inline LeanSelectable_t2376D6100D5CC4EEC7F3BB4A3B0D57810CBDA1D9 * get_RequiredSelectable_6() const { return ___RequiredSelectable_6; }
	inline LeanSelectable_t2376D6100D5CC4EEC7F3BB4A3B0D57810CBDA1D9 ** get_address_of_RequiredSelectable_6() { return &___RequiredSelectable_6; }
	inline void set_RequiredSelectable_6(LeanSelectable_t2376D6100D5CC4EEC7F3BB4A3B0D57810CBDA1D9 * value)
	{
		___RequiredSelectable_6 = value;
		Il2CppCodeGenWriteBarrier((&___RequiredSelectable_6), value);
	}

	inline static int32_t get_offset_of_WheelSensitivity_7() { return static_cast<int32_t>(offsetof(LeanScale_tDB8973F254335EC3C2393B21E572A79BC89C5D7C, ___WheelSensitivity_7)); }
	inline float get_WheelSensitivity_7() const { return ___WheelSensitivity_7; }
	inline float* get_address_of_WheelSensitivity_7() { return &___WheelSensitivity_7; }
	inline void set_WheelSensitivity_7(float value)
	{
		___WheelSensitivity_7 = value;
	}

	inline static int32_t get_offset_of_Camera_8() { return static_cast<int32_t>(offsetof(LeanScale_tDB8973F254335EC3C2393B21E572A79BC89C5D7C, ___Camera_8)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_Camera_8() const { return ___Camera_8; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_Camera_8() { return &___Camera_8; }
	inline void set_Camera_8(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___Camera_8 = value;
		Il2CppCodeGenWriteBarrier((&___Camera_8), value);
	}

	inline static int32_t get_offset_of_Relative_9() { return static_cast<int32_t>(offsetof(LeanScale_tDB8973F254335EC3C2393B21E572A79BC89C5D7C, ___Relative_9)); }
	inline bool get_Relative_9() const { return ___Relative_9; }
	inline bool* get_address_of_Relative_9() { return &___Relative_9; }
	inline void set_Relative_9(bool value)
	{
		___Relative_9 = value;
	}

	inline static int32_t get_offset_of_ScaleClamp_10() { return static_cast<int32_t>(offsetof(LeanScale_tDB8973F254335EC3C2393B21E572A79BC89C5D7C, ___ScaleClamp_10)); }
	inline bool get_ScaleClamp_10() const { return ___ScaleClamp_10; }
	inline bool* get_address_of_ScaleClamp_10() { return &___ScaleClamp_10; }
	inline void set_ScaleClamp_10(bool value)
	{
		___ScaleClamp_10 = value;
	}

	inline static int32_t get_offset_of_ScaleMin_11() { return static_cast<int32_t>(offsetof(LeanScale_tDB8973F254335EC3C2393B21E572A79BC89C5D7C, ___ScaleMin_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_ScaleMin_11() const { return ___ScaleMin_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_ScaleMin_11() { return &___ScaleMin_11; }
	inline void set_ScaleMin_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___ScaleMin_11 = value;
	}

	inline static int32_t get_offset_of_ScaleMax_12() { return static_cast<int32_t>(offsetof(LeanScale_tDB8973F254335EC3C2393B21E572A79BC89C5D7C, ___ScaleMax_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_ScaleMax_12() const { return ___ScaleMax_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_ScaleMax_12() { return &___ScaleMax_12; }
	inline void set_ScaleMax_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___ScaleMax_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEANSCALE_TDB8973F254335EC3C2393B21E572A79BC89C5D7C_H
#ifndef LEANSELECT_T02A1E0856DC63195FF3466117DE7D0A64047B60E_H
#define LEANSELECT_T02A1E0856DC63195FF3466117DE7D0A64047B60E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lean.Touch.LeanSelect
struct  LeanSelect_t02A1E0856DC63195FF3466117DE7D0A64047B60E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Lean.Touch.LeanSelect_SelectType Lean.Touch.LeanSelect::SelectUsing
	int32_t ___SelectUsing_4;
	// UnityEngine.LayerMask Lean.Touch.LeanSelect::LayerMask
	LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  ___LayerMask_5;
	// Lean.Touch.LeanSelect_SearchType Lean.Touch.LeanSelect::Search
	int32_t ___Search_6;
	// Lean.Touch.LeanSelectable Lean.Touch.LeanSelect::CurrentSelectable
	LeanSelectable_t2376D6100D5CC4EEC7F3BB4A3B0D57810CBDA1D9 * ___CurrentSelectable_7;
	// Lean.Touch.LeanSelect_ReselectType Lean.Touch.LeanSelect::Reselect
	int32_t ___Reselect_8;
	// System.Boolean Lean.Touch.LeanSelect::AutoDeselect
	bool ___AutoDeselect_9;

public:
	inline static int32_t get_offset_of_SelectUsing_4() { return static_cast<int32_t>(offsetof(LeanSelect_t02A1E0856DC63195FF3466117DE7D0A64047B60E, ___SelectUsing_4)); }
	inline int32_t get_SelectUsing_4() const { return ___SelectUsing_4; }
	inline int32_t* get_address_of_SelectUsing_4() { return &___SelectUsing_4; }
	inline void set_SelectUsing_4(int32_t value)
	{
		___SelectUsing_4 = value;
	}

	inline static int32_t get_offset_of_LayerMask_5() { return static_cast<int32_t>(offsetof(LeanSelect_t02A1E0856DC63195FF3466117DE7D0A64047B60E, ___LayerMask_5)); }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  get_LayerMask_5() const { return ___LayerMask_5; }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0 * get_address_of_LayerMask_5() { return &___LayerMask_5; }
	inline void set_LayerMask_5(LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  value)
	{
		___LayerMask_5 = value;
	}

	inline static int32_t get_offset_of_Search_6() { return static_cast<int32_t>(offsetof(LeanSelect_t02A1E0856DC63195FF3466117DE7D0A64047B60E, ___Search_6)); }
	inline int32_t get_Search_6() const { return ___Search_6; }
	inline int32_t* get_address_of_Search_6() { return &___Search_6; }
	inline void set_Search_6(int32_t value)
	{
		___Search_6 = value;
	}

	inline static int32_t get_offset_of_CurrentSelectable_7() { return static_cast<int32_t>(offsetof(LeanSelect_t02A1E0856DC63195FF3466117DE7D0A64047B60E, ___CurrentSelectable_7)); }
	inline LeanSelectable_t2376D6100D5CC4EEC7F3BB4A3B0D57810CBDA1D9 * get_CurrentSelectable_7() const { return ___CurrentSelectable_7; }
	inline LeanSelectable_t2376D6100D5CC4EEC7F3BB4A3B0D57810CBDA1D9 ** get_address_of_CurrentSelectable_7() { return &___CurrentSelectable_7; }
	inline void set_CurrentSelectable_7(LeanSelectable_t2376D6100D5CC4EEC7F3BB4A3B0D57810CBDA1D9 * value)
	{
		___CurrentSelectable_7 = value;
		Il2CppCodeGenWriteBarrier((&___CurrentSelectable_7), value);
	}

	inline static int32_t get_offset_of_Reselect_8() { return static_cast<int32_t>(offsetof(LeanSelect_t02A1E0856DC63195FF3466117DE7D0A64047B60E, ___Reselect_8)); }
	inline int32_t get_Reselect_8() const { return ___Reselect_8; }
	inline int32_t* get_address_of_Reselect_8() { return &___Reselect_8; }
	inline void set_Reselect_8(int32_t value)
	{
		___Reselect_8 = value;
	}

	inline static int32_t get_offset_of_AutoDeselect_9() { return static_cast<int32_t>(offsetof(LeanSelect_t02A1E0856DC63195FF3466117DE7D0A64047B60E, ___AutoDeselect_9)); }
	inline bool get_AutoDeselect_9() const { return ___AutoDeselect_9; }
	inline bool* get_address_of_AutoDeselect_9() { return &___AutoDeselect_9; }
	inline void set_AutoDeselect_9(bool value)
	{
		___AutoDeselect_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEANSELECT_T02A1E0856DC63195FF3466117DE7D0A64047B60E_H
#ifndef LEANSELECTABLE_T2376D6100D5CC4EEC7F3BB4A3B0D57810CBDA1D9_H
#define LEANSELECTABLE_T2376D6100D5CC4EEC7F3BB4A3B0D57810CBDA1D9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lean.Touch.LeanSelectable
struct  LeanSelectable_t2376D6100D5CC4EEC7F3BB4A3B0D57810CBDA1D9  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean Lean.Touch.LeanSelectable::HideWithFinger
	bool ___HideWithFinger_4;
	// Lean.Touch.LeanFinger Lean.Touch.LeanSelectable::SelectingFinger
	LeanFinger_t14B8CB80FF55DDB97259D004E0F1149542C099BD * ___SelectingFinger_5;
	// Lean.Touch.LeanSelectable_LeanFingerEvent Lean.Touch.LeanSelectable::OnSelect
	LeanFingerEvent_t1F6EB317C16105591BB549C360BDBE8E67911B5D * ___OnSelect_6;
	// Lean.Touch.LeanSelectable_LeanFingerEvent Lean.Touch.LeanSelectable::OnSelectUp
	LeanFingerEvent_t1F6EB317C16105591BB549C360BDBE8E67911B5D * ___OnSelectUp_7;
	// UnityEngine.Events.UnityEvent Lean.Touch.LeanSelectable::OnDeselect
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ___OnDeselect_8;
	// System.Boolean Lean.Touch.LeanSelectable::isSelected
	bool ___isSelected_9;

public:
	inline static int32_t get_offset_of_HideWithFinger_4() { return static_cast<int32_t>(offsetof(LeanSelectable_t2376D6100D5CC4EEC7F3BB4A3B0D57810CBDA1D9, ___HideWithFinger_4)); }
	inline bool get_HideWithFinger_4() const { return ___HideWithFinger_4; }
	inline bool* get_address_of_HideWithFinger_4() { return &___HideWithFinger_4; }
	inline void set_HideWithFinger_4(bool value)
	{
		___HideWithFinger_4 = value;
	}

	inline static int32_t get_offset_of_SelectingFinger_5() { return static_cast<int32_t>(offsetof(LeanSelectable_t2376D6100D5CC4EEC7F3BB4A3B0D57810CBDA1D9, ___SelectingFinger_5)); }
	inline LeanFinger_t14B8CB80FF55DDB97259D004E0F1149542C099BD * get_SelectingFinger_5() const { return ___SelectingFinger_5; }
	inline LeanFinger_t14B8CB80FF55DDB97259D004E0F1149542C099BD ** get_address_of_SelectingFinger_5() { return &___SelectingFinger_5; }
	inline void set_SelectingFinger_5(LeanFinger_t14B8CB80FF55DDB97259D004E0F1149542C099BD * value)
	{
		___SelectingFinger_5 = value;
		Il2CppCodeGenWriteBarrier((&___SelectingFinger_5), value);
	}

	inline static int32_t get_offset_of_OnSelect_6() { return static_cast<int32_t>(offsetof(LeanSelectable_t2376D6100D5CC4EEC7F3BB4A3B0D57810CBDA1D9, ___OnSelect_6)); }
	inline LeanFingerEvent_t1F6EB317C16105591BB549C360BDBE8E67911B5D * get_OnSelect_6() const { return ___OnSelect_6; }
	inline LeanFingerEvent_t1F6EB317C16105591BB549C360BDBE8E67911B5D ** get_address_of_OnSelect_6() { return &___OnSelect_6; }
	inline void set_OnSelect_6(LeanFingerEvent_t1F6EB317C16105591BB549C360BDBE8E67911B5D * value)
	{
		___OnSelect_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnSelect_6), value);
	}

	inline static int32_t get_offset_of_OnSelectUp_7() { return static_cast<int32_t>(offsetof(LeanSelectable_t2376D6100D5CC4EEC7F3BB4A3B0D57810CBDA1D9, ___OnSelectUp_7)); }
	inline LeanFingerEvent_t1F6EB317C16105591BB549C360BDBE8E67911B5D * get_OnSelectUp_7() const { return ___OnSelectUp_7; }
	inline LeanFingerEvent_t1F6EB317C16105591BB549C360BDBE8E67911B5D ** get_address_of_OnSelectUp_7() { return &___OnSelectUp_7; }
	inline void set_OnSelectUp_7(LeanFingerEvent_t1F6EB317C16105591BB549C360BDBE8E67911B5D * value)
	{
		___OnSelectUp_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnSelectUp_7), value);
	}

	inline static int32_t get_offset_of_OnDeselect_8() { return static_cast<int32_t>(offsetof(LeanSelectable_t2376D6100D5CC4EEC7F3BB4A3B0D57810CBDA1D9, ___OnDeselect_8)); }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * get_OnDeselect_8() const { return ___OnDeselect_8; }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F ** get_address_of_OnDeselect_8() { return &___OnDeselect_8; }
	inline void set_OnDeselect_8(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * value)
	{
		___OnDeselect_8 = value;
		Il2CppCodeGenWriteBarrier((&___OnDeselect_8), value);
	}

	inline static int32_t get_offset_of_isSelected_9() { return static_cast<int32_t>(offsetof(LeanSelectable_t2376D6100D5CC4EEC7F3BB4A3B0D57810CBDA1D9, ___isSelected_9)); }
	inline bool get_isSelected_9() const { return ___isSelected_9; }
	inline bool* get_address_of_isSelected_9() { return &___isSelected_9; }
	inline void set_isSelected_9(bool value)
	{
		___isSelected_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEANSELECTABLE_T2376D6100D5CC4EEC7F3BB4A3B0D57810CBDA1D9_H
#ifndef LEANSELECTABLEBEHAVIOUR_T2F39190FE17A2E1DC2DC7772172DA1E14D3BB6FF_H
#define LEANSELECTABLEBEHAVIOUR_T2F39190FE17A2E1DC2DC7772172DA1E14D3BB6FF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lean.Touch.LeanSelectableBehaviour
struct  LeanSelectableBehaviour_t2F39190FE17A2E1DC2DC7772172DA1E14D3BB6FF  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Lean.Touch.LeanSelectable Lean.Touch.LeanSelectableBehaviour::selectable
	LeanSelectable_t2376D6100D5CC4EEC7F3BB4A3B0D57810CBDA1D9 * ___selectable_4;

public:
	inline static int32_t get_offset_of_selectable_4() { return static_cast<int32_t>(offsetof(LeanSelectableBehaviour_t2F39190FE17A2E1DC2DC7772172DA1E14D3BB6FF, ___selectable_4)); }
	inline LeanSelectable_t2376D6100D5CC4EEC7F3BB4A3B0D57810CBDA1D9 * get_selectable_4() const { return ___selectable_4; }
	inline LeanSelectable_t2376D6100D5CC4EEC7F3BB4A3B0D57810CBDA1D9 ** get_address_of_selectable_4() { return &___selectable_4; }
	inline void set_selectable_4(LeanSelectable_t2376D6100D5CC4EEC7F3BB4A3B0D57810CBDA1D9 * value)
	{
		___selectable_4 = value;
		Il2CppCodeGenWriteBarrier((&___selectable_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEANSELECTABLEBEHAVIOUR_T2F39190FE17A2E1DC2DC7772172DA1E14D3BB6FF_H
#ifndef LEANSPAWN_T30A01E3156098F1CF384DBA219F45B0D03F023CC_H
#define LEANSPAWN_T30A01E3156098F1CF384DBA219F45B0D03F023CC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lean.Touch.LeanSpawn
struct  LeanSpawn_t30A01E3156098F1CF384DBA219F45B0D03F023CC  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Transform Lean.Touch.LeanSpawn::Prefab
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___Prefab_4;
	// System.Single Lean.Touch.LeanSpawn::Distance
	float ___Distance_5;

public:
	inline static int32_t get_offset_of_Prefab_4() { return static_cast<int32_t>(offsetof(LeanSpawn_t30A01E3156098F1CF384DBA219F45B0D03F023CC, ___Prefab_4)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_Prefab_4() const { return ___Prefab_4; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_Prefab_4() { return &___Prefab_4; }
	inline void set_Prefab_4(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___Prefab_4 = value;
		Il2CppCodeGenWriteBarrier((&___Prefab_4), value);
	}

	inline static int32_t get_offset_of_Distance_5() { return static_cast<int32_t>(offsetof(LeanSpawn_t30A01E3156098F1CF384DBA219F45B0D03F023CC, ___Distance_5)); }
	inline float get_Distance_5() const { return ___Distance_5; }
	inline float* get_address_of_Distance_5() { return &___Distance_5; }
	inline void set_Distance_5(float value)
	{
		___Distance_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEANSPAWN_T30A01E3156098F1CF384DBA219F45B0D03F023CC_H
#ifndef LEANSWIPEDIRECTION4_T1C1E954410E25902BD918BD511EB38F89E7FF28B_H
#define LEANSWIPEDIRECTION4_T1C1E954410E25902BD918BD511EB38F89E7FF28B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lean.Touch.LeanSwipeDirection4
struct  LeanSwipeDirection4_t1C1E954410E25902BD918BD511EB38F89E7FF28B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text Lean.Touch.LeanSwipeDirection4::InfoText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___InfoText_4;

public:
	inline static int32_t get_offset_of_InfoText_4() { return static_cast<int32_t>(offsetof(LeanSwipeDirection4_t1C1E954410E25902BD918BD511EB38F89E7FF28B, ___InfoText_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_InfoText_4() const { return ___InfoText_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_InfoText_4() { return &___InfoText_4; }
	inline void set_InfoText_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___InfoText_4 = value;
		Il2CppCodeGenWriteBarrier((&___InfoText_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEANSWIPEDIRECTION4_T1C1E954410E25902BD918BD511EB38F89E7FF28B_H
#ifndef LEANSWIPEDIRECTION8_T21FC188D5119A3B629E0E47CCC19F9CD9AD5D1C9_H
#define LEANSWIPEDIRECTION8_T21FC188D5119A3B629E0E47CCC19F9CD9AD5D1C9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lean.Touch.LeanSwipeDirection8
struct  LeanSwipeDirection8_t21FC188D5119A3B629E0E47CCC19F9CD9AD5D1C9  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text Lean.Touch.LeanSwipeDirection8::InfoText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___InfoText_4;

public:
	inline static int32_t get_offset_of_InfoText_4() { return static_cast<int32_t>(offsetof(LeanSwipeDirection8_t21FC188D5119A3B629E0E47CCC19F9CD9AD5D1C9, ___InfoText_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_InfoText_4() const { return ___InfoText_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_InfoText_4() { return &___InfoText_4; }
	inline void set_InfoText_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___InfoText_4 = value;
		Il2CppCodeGenWriteBarrier((&___InfoText_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEANSWIPEDIRECTION8_T21FC188D5119A3B629E0E47CCC19F9CD9AD5D1C9_H
#ifndef LEANSWIPERIGIDBODY2D_T9ECD41C52094717AEECDAD91E651FD46147655EF_H
#define LEANSWIPERIGIDBODY2D_T9ECD41C52094717AEECDAD91E651FD46147655EF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lean.Touch.LeanSwipeRigidbody2D
struct  LeanSwipeRigidbody2D_t9ECD41C52094717AEECDAD91E651FD46147655EF  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.LayerMask Lean.Touch.LeanSwipeRigidbody2D::LayerMask
	LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  ___LayerMask_4;
	// System.Single Lean.Touch.LeanSwipeRigidbody2D::ForceMultiplier
	float ___ForceMultiplier_5;

public:
	inline static int32_t get_offset_of_LayerMask_4() { return static_cast<int32_t>(offsetof(LeanSwipeRigidbody2D_t9ECD41C52094717AEECDAD91E651FD46147655EF, ___LayerMask_4)); }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  get_LayerMask_4() const { return ___LayerMask_4; }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0 * get_address_of_LayerMask_4() { return &___LayerMask_4; }
	inline void set_LayerMask_4(LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  value)
	{
		___LayerMask_4 = value;
	}

	inline static int32_t get_offset_of_ForceMultiplier_5() { return static_cast<int32_t>(offsetof(LeanSwipeRigidbody2D_t9ECD41C52094717AEECDAD91E651FD46147655EF, ___ForceMultiplier_5)); }
	inline float get_ForceMultiplier_5() const { return ___ForceMultiplier_5; }
	inline float* get_address_of_ForceMultiplier_5() { return &___ForceMultiplier_5; }
	inline void set_ForceMultiplier_5(float value)
	{
		___ForceMultiplier_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEANSWIPERIGIDBODY2D_T9ECD41C52094717AEECDAD91E651FD46147655EF_H
#ifndef LEANSWIPERIGIDBODY2DNORELEASE_TF6F8D1327109A4B89259DAC85D3AE3AAC6A81F32_H
#define LEANSWIPERIGIDBODY2DNORELEASE_TF6F8D1327109A4B89259DAC85D3AE3AAC6A81F32_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lean.Touch.LeanSwipeRigidbody2DNoRelease
struct  LeanSwipeRigidbody2DNoRelease_tF6F8D1327109A4B89259DAC85D3AE3AAC6A81F32  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.LayerMask Lean.Touch.LeanSwipeRigidbody2DNoRelease::LayerMask
	LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  ___LayerMask_4;
	// System.Single Lean.Touch.LeanSwipeRigidbody2DNoRelease::ImpulseForce
	float ___ImpulseForce_5;
	// Lean.Touch.LeanFinger Lean.Touch.LeanSwipeRigidbody2DNoRelease::swipingFinger
	LeanFinger_t14B8CB80FF55DDB97259D004E0F1149542C099BD * ___swipingFinger_6;

public:
	inline static int32_t get_offset_of_LayerMask_4() { return static_cast<int32_t>(offsetof(LeanSwipeRigidbody2DNoRelease_tF6F8D1327109A4B89259DAC85D3AE3AAC6A81F32, ___LayerMask_4)); }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  get_LayerMask_4() const { return ___LayerMask_4; }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0 * get_address_of_LayerMask_4() { return &___LayerMask_4; }
	inline void set_LayerMask_4(LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  value)
	{
		___LayerMask_4 = value;
	}

	inline static int32_t get_offset_of_ImpulseForce_5() { return static_cast<int32_t>(offsetof(LeanSwipeRigidbody2DNoRelease_tF6F8D1327109A4B89259DAC85D3AE3AAC6A81F32, ___ImpulseForce_5)); }
	inline float get_ImpulseForce_5() const { return ___ImpulseForce_5; }
	inline float* get_address_of_ImpulseForce_5() { return &___ImpulseForce_5; }
	inline void set_ImpulseForce_5(float value)
	{
		___ImpulseForce_5 = value;
	}

	inline static int32_t get_offset_of_swipingFinger_6() { return static_cast<int32_t>(offsetof(LeanSwipeRigidbody2DNoRelease_tF6F8D1327109A4B89259DAC85D3AE3AAC6A81F32, ___swipingFinger_6)); }
	inline LeanFinger_t14B8CB80FF55DDB97259D004E0F1149542C099BD * get_swipingFinger_6() const { return ___swipingFinger_6; }
	inline LeanFinger_t14B8CB80FF55DDB97259D004E0F1149542C099BD ** get_address_of_swipingFinger_6() { return &___swipingFinger_6; }
	inline void set_swipingFinger_6(LeanFinger_t14B8CB80FF55DDB97259D004E0F1149542C099BD * value)
	{
		___swipingFinger_6 = value;
		Il2CppCodeGenWriteBarrier((&___swipingFinger_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEANSWIPERIGIDBODY2DNORELEASE_TF6F8D1327109A4B89259DAC85D3AE3AAC6A81F32_H
#ifndef LEANSWIPERIGIDBODY3D_T9635B49848BD5633E8D0BBBC65AD3F3F203846F8_H
#define LEANSWIPERIGIDBODY3D_T9635B49848BD5633E8D0BBBC65AD3F3F203846F8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lean.Touch.LeanSwipeRigidbody3D
struct  LeanSwipeRigidbody3D_t9635B49848BD5633E8D0BBBC65AD3F3F203846F8  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.LayerMask Lean.Touch.LeanSwipeRigidbody3D::LayerMask
	LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  ___LayerMask_4;
	// System.Single Lean.Touch.LeanSwipeRigidbody3D::ForceMultiplier
	float ___ForceMultiplier_5;

public:
	inline static int32_t get_offset_of_LayerMask_4() { return static_cast<int32_t>(offsetof(LeanSwipeRigidbody3D_t9635B49848BD5633E8D0BBBC65AD3F3F203846F8, ___LayerMask_4)); }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  get_LayerMask_4() const { return ___LayerMask_4; }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0 * get_address_of_LayerMask_4() { return &___LayerMask_4; }
	inline void set_LayerMask_4(LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  value)
	{
		___LayerMask_4 = value;
	}

	inline static int32_t get_offset_of_ForceMultiplier_5() { return static_cast<int32_t>(offsetof(LeanSwipeRigidbody3D_t9635B49848BD5633E8D0BBBC65AD3F3F203846F8, ___ForceMultiplier_5)); }
	inline float get_ForceMultiplier_5() const { return ___ForceMultiplier_5; }
	inline float* get_address_of_ForceMultiplier_5() { return &___ForceMultiplier_5; }
	inline void set_ForceMultiplier_5(float value)
	{
		___ForceMultiplier_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEANSWIPERIGIDBODY3D_T9635B49848BD5633E8D0BBBC65AD3F3F203846F8_H
#ifndef LEANSWIPERIGIDBODY3DNORELEASE_T7440DD6AD655BABA1D0B20A6BCA2BCF4D327C823_H
#define LEANSWIPERIGIDBODY3DNORELEASE_T7440DD6AD655BABA1D0B20A6BCA2BCF4D327C823_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lean.Touch.LeanSwipeRigidbody3DNoRelease
struct  LeanSwipeRigidbody3DNoRelease_t7440DD6AD655BABA1D0B20A6BCA2BCF4D327C823  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.LayerMask Lean.Touch.LeanSwipeRigidbody3DNoRelease::LayerMask
	LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  ___LayerMask_4;
	// System.Single Lean.Touch.LeanSwipeRigidbody3DNoRelease::ImpulseForce
	float ___ImpulseForce_5;
	// Lean.Touch.LeanFinger Lean.Touch.LeanSwipeRigidbody3DNoRelease::swipingFinger
	LeanFinger_t14B8CB80FF55DDB97259D004E0F1149542C099BD * ___swipingFinger_6;

public:
	inline static int32_t get_offset_of_LayerMask_4() { return static_cast<int32_t>(offsetof(LeanSwipeRigidbody3DNoRelease_t7440DD6AD655BABA1D0B20A6BCA2BCF4D327C823, ___LayerMask_4)); }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  get_LayerMask_4() const { return ___LayerMask_4; }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0 * get_address_of_LayerMask_4() { return &___LayerMask_4; }
	inline void set_LayerMask_4(LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  value)
	{
		___LayerMask_4 = value;
	}

	inline static int32_t get_offset_of_ImpulseForce_5() { return static_cast<int32_t>(offsetof(LeanSwipeRigidbody3DNoRelease_t7440DD6AD655BABA1D0B20A6BCA2BCF4D327C823, ___ImpulseForce_5)); }
	inline float get_ImpulseForce_5() const { return ___ImpulseForce_5; }
	inline float* get_address_of_ImpulseForce_5() { return &___ImpulseForce_5; }
	inline void set_ImpulseForce_5(float value)
	{
		___ImpulseForce_5 = value;
	}

	inline static int32_t get_offset_of_swipingFinger_6() { return static_cast<int32_t>(offsetof(LeanSwipeRigidbody3DNoRelease_t7440DD6AD655BABA1D0B20A6BCA2BCF4D327C823, ___swipingFinger_6)); }
	inline LeanFinger_t14B8CB80FF55DDB97259D004E0F1149542C099BD * get_swipingFinger_6() const { return ___swipingFinger_6; }
	inline LeanFinger_t14B8CB80FF55DDB97259D004E0F1149542C099BD ** get_address_of_swipingFinger_6() { return &___swipingFinger_6; }
	inline void set_swipingFinger_6(LeanFinger_t14B8CB80FF55DDB97259D004E0F1149542C099BD * value)
	{
		___swipingFinger_6 = value;
		Il2CppCodeGenWriteBarrier((&___swipingFinger_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEANSWIPERIGIDBODY3DNORELEASE_T7440DD6AD655BABA1D0B20A6BCA2BCF4D327C823_H
#ifndef LEANTOUCH_T04E4C85F6ED9E388AD8D7FA10A67E9C74701F15F_H
#define LEANTOUCH_T04E4C85F6ED9E388AD8D7FA10A67E9C74701F15F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lean.Touch.LeanTouch
struct  LeanTouch_t04E4C85F6ED9E388AD8D7FA10A67E9C74701F15F  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single Lean.Touch.LeanTouch::TapThreshold
	float ___TapThreshold_13;
	// System.Single Lean.Touch.LeanTouch::SwipeThreshold
	float ___SwipeThreshold_15;
	// System.Int32 Lean.Touch.LeanTouch::ReferenceDpi
	int32_t ___ReferenceDpi_17;
	// UnityEngine.LayerMask Lean.Touch.LeanTouch::GuiLayers
	LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  ___GuiLayers_19;
	// System.Boolean Lean.Touch.LeanTouch::RecordFingers
	bool ___RecordFingers_20;
	// System.Single Lean.Touch.LeanTouch::RecordThreshold
	float ___RecordThreshold_21;
	// System.Single Lean.Touch.LeanTouch::RecordLimit
	float ___RecordLimit_22;
	// System.Boolean Lean.Touch.LeanTouch::SimulateMultiFingers
	bool ___SimulateMultiFingers_23;
	// UnityEngine.KeyCode Lean.Touch.LeanTouch::PinchTwistKey
	int32_t ___PinchTwistKey_24;
	// UnityEngine.KeyCode Lean.Touch.LeanTouch::MultiDragKey
	int32_t ___MultiDragKey_25;
	// UnityEngine.Texture2D Lean.Touch.LeanTouch::FingerTexture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___FingerTexture_26;

public:
	inline static int32_t get_offset_of_TapThreshold_13() { return static_cast<int32_t>(offsetof(LeanTouch_t04E4C85F6ED9E388AD8D7FA10A67E9C74701F15F, ___TapThreshold_13)); }
	inline float get_TapThreshold_13() const { return ___TapThreshold_13; }
	inline float* get_address_of_TapThreshold_13() { return &___TapThreshold_13; }
	inline void set_TapThreshold_13(float value)
	{
		___TapThreshold_13 = value;
	}

	inline static int32_t get_offset_of_SwipeThreshold_15() { return static_cast<int32_t>(offsetof(LeanTouch_t04E4C85F6ED9E388AD8D7FA10A67E9C74701F15F, ___SwipeThreshold_15)); }
	inline float get_SwipeThreshold_15() const { return ___SwipeThreshold_15; }
	inline float* get_address_of_SwipeThreshold_15() { return &___SwipeThreshold_15; }
	inline void set_SwipeThreshold_15(float value)
	{
		___SwipeThreshold_15 = value;
	}

	inline static int32_t get_offset_of_ReferenceDpi_17() { return static_cast<int32_t>(offsetof(LeanTouch_t04E4C85F6ED9E388AD8D7FA10A67E9C74701F15F, ___ReferenceDpi_17)); }
	inline int32_t get_ReferenceDpi_17() const { return ___ReferenceDpi_17; }
	inline int32_t* get_address_of_ReferenceDpi_17() { return &___ReferenceDpi_17; }
	inline void set_ReferenceDpi_17(int32_t value)
	{
		___ReferenceDpi_17 = value;
	}

	inline static int32_t get_offset_of_GuiLayers_19() { return static_cast<int32_t>(offsetof(LeanTouch_t04E4C85F6ED9E388AD8D7FA10A67E9C74701F15F, ___GuiLayers_19)); }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  get_GuiLayers_19() const { return ___GuiLayers_19; }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0 * get_address_of_GuiLayers_19() { return &___GuiLayers_19; }
	inline void set_GuiLayers_19(LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  value)
	{
		___GuiLayers_19 = value;
	}

	inline static int32_t get_offset_of_RecordFingers_20() { return static_cast<int32_t>(offsetof(LeanTouch_t04E4C85F6ED9E388AD8D7FA10A67E9C74701F15F, ___RecordFingers_20)); }
	inline bool get_RecordFingers_20() const { return ___RecordFingers_20; }
	inline bool* get_address_of_RecordFingers_20() { return &___RecordFingers_20; }
	inline void set_RecordFingers_20(bool value)
	{
		___RecordFingers_20 = value;
	}

	inline static int32_t get_offset_of_RecordThreshold_21() { return static_cast<int32_t>(offsetof(LeanTouch_t04E4C85F6ED9E388AD8D7FA10A67E9C74701F15F, ___RecordThreshold_21)); }
	inline float get_RecordThreshold_21() const { return ___RecordThreshold_21; }
	inline float* get_address_of_RecordThreshold_21() { return &___RecordThreshold_21; }
	inline void set_RecordThreshold_21(float value)
	{
		___RecordThreshold_21 = value;
	}

	inline static int32_t get_offset_of_RecordLimit_22() { return static_cast<int32_t>(offsetof(LeanTouch_t04E4C85F6ED9E388AD8D7FA10A67E9C74701F15F, ___RecordLimit_22)); }
	inline float get_RecordLimit_22() const { return ___RecordLimit_22; }
	inline float* get_address_of_RecordLimit_22() { return &___RecordLimit_22; }
	inline void set_RecordLimit_22(float value)
	{
		___RecordLimit_22 = value;
	}

	inline static int32_t get_offset_of_SimulateMultiFingers_23() { return static_cast<int32_t>(offsetof(LeanTouch_t04E4C85F6ED9E388AD8D7FA10A67E9C74701F15F, ___SimulateMultiFingers_23)); }
	inline bool get_SimulateMultiFingers_23() const { return ___SimulateMultiFingers_23; }
	inline bool* get_address_of_SimulateMultiFingers_23() { return &___SimulateMultiFingers_23; }
	inline void set_SimulateMultiFingers_23(bool value)
	{
		___SimulateMultiFingers_23 = value;
	}

	inline static int32_t get_offset_of_PinchTwistKey_24() { return static_cast<int32_t>(offsetof(LeanTouch_t04E4C85F6ED9E388AD8D7FA10A67E9C74701F15F, ___PinchTwistKey_24)); }
	inline int32_t get_PinchTwistKey_24() const { return ___PinchTwistKey_24; }
	inline int32_t* get_address_of_PinchTwistKey_24() { return &___PinchTwistKey_24; }
	inline void set_PinchTwistKey_24(int32_t value)
	{
		___PinchTwistKey_24 = value;
	}

	inline static int32_t get_offset_of_MultiDragKey_25() { return static_cast<int32_t>(offsetof(LeanTouch_t04E4C85F6ED9E388AD8D7FA10A67E9C74701F15F, ___MultiDragKey_25)); }
	inline int32_t get_MultiDragKey_25() const { return ___MultiDragKey_25; }
	inline int32_t* get_address_of_MultiDragKey_25() { return &___MultiDragKey_25; }
	inline void set_MultiDragKey_25(int32_t value)
	{
		___MultiDragKey_25 = value;
	}

	inline static int32_t get_offset_of_FingerTexture_26() { return static_cast<int32_t>(offsetof(LeanTouch_t04E4C85F6ED9E388AD8D7FA10A67E9C74701F15F, ___FingerTexture_26)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_FingerTexture_26() const { return ___FingerTexture_26; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_FingerTexture_26() { return &___FingerTexture_26; }
	inline void set_FingerTexture_26(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___FingerTexture_26 = value;
		Il2CppCodeGenWriteBarrier((&___FingerTexture_26), value);
	}
};

struct LeanTouch_t04E4C85F6ED9E388AD8D7FA10A67E9C74701F15F_StaticFields
{
public:
	// System.Collections.Generic.List`1<Lean.Touch.LeanTouch> Lean.Touch.LeanTouch::Instances
	List_1_t740D263DA628BB692D9D7C9A9E75369CB3FEA371 * ___Instances_4;
	// System.Collections.Generic.List`1<Lean.Touch.LeanFinger> Lean.Touch.LeanTouch::Fingers
	List_1_tDA85F67BB9390A7D9940CB6D1B08D2CE04BCE37E * ___Fingers_5;
	// System.Collections.Generic.List`1<Lean.Touch.LeanFinger> Lean.Touch.LeanTouch::InactiveFingers
	List_1_tDA85F67BB9390A7D9940CB6D1B08D2CE04BCE37E * ___InactiveFingers_6;
	// System.Action`1<Lean.Touch.LeanFinger> Lean.Touch.LeanTouch::OnFingerDown
	Action_1_t955501676F4CE8E1E3A8F8000378BDBB68BCD2EC * ___OnFingerDown_7;
	// System.Action`1<Lean.Touch.LeanFinger> Lean.Touch.LeanTouch::OnFingerSet
	Action_1_t955501676F4CE8E1E3A8F8000378BDBB68BCD2EC * ___OnFingerSet_8;
	// System.Action`1<Lean.Touch.LeanFinger> Lean.Touch.LeanTouch::OnFingerUp
	Action_1_t955501676F4CE8E1E3A8F8000378BDBB68BCD2EC * ___OnFingerUp_9;
	// System.Action`1<Lean.Touch.LeanFinger> Lean.Touch.LeanTouch::OnFingerTap
	Action_1_t955501676F4CE8E1E3A8F8000378BDBB68BCD2EC * ___OnFingerTap_10;
	// System.Action`1<Lean.Touch.LeanFinger> Lean.Touch.LeanTouch::OnFingerSwipe
	Action_1_t955501676F4CE8E1E3A8F8000378BDBB68BCD2EC * ___OnFingerSwipe_11;
	// System.Action`1<System.Collections.Generic.List`1<Lean.Touch.LeanFinger>> Lean.Touch.LeanTouch::OnGesture
	Action_1_t506E130BAFC89D90111C6710E76354559705EEC2 * ___OnGesture_12;
	// System.Int32 Lean.Touch.LeanTouch::highestMouseButton
	int32_t ___highestMouseButton_27;
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult> Lean.Touch.LeanTouch::tempRaycastResults
	List_1_tB291263EEE72B9F137CA4DC19F039DE672D08028 * ___tempRaycastResults_28;
	// System.Collections.Generic.List`1<Lean.Touch.LeanFinger> Lean.Touch.LeanTouch::filteredFingers
	List_1_tDA85F67BB9390A7D9940CB6D1B08D2CE04BCE37E * ___filteredFingers_29;
	// UnityEngine.EventSystems.PointerEventData Lean.Touch.LeanTouch::tempPointerEventData
	PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63 * ___tempPointerEventData_30;
	// UnityEngine.EventSystems.EventSystem Lean.Touch.LeanTouch::tempEventSystem
	EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 * ___tempEventSystem_31;

public:
	inline static int32_t get_offset_of_Instances_4() { return static_cast<int32_t>(offsetof(LeanTouch_t04E4C85F6ED9E388AD8D7FA10A67E9C74701F15F_StaticFields, ___Instances_4)); }
	inline List_1_t740D263DA628BB692D9D7C9A9E75369CB3FEA371 * get_Instances_4() const { return ___Instances_4; }
	inline List_1_t740D263DA628BB692D9D7C9A9E75369CB3FEA371 ** get_address_of_Instances_4() { return &___Instances_4; }
	inline void set_Instances_4(List_1_t740D263DA628BB692D9D7C9A9E75369CB3FEA371 * value)
	{
		___Instances_4 = value;
		Il2CppCodeGenWriteBarrier((&___Instances_4), value);
	}

	inline static int32_t get_offset_of_Fingers_5() { return static_cast<int32_t>(offsetof(LeanTouch_t04E4C85F6ED9E388AD8D7FA10A67E9C74701F15F_StaticFields, ___Fingers_5)); }
	inline List_1_tDA85F67BB9390A7D9940CB6D1B08D2CE04BCE37E * get_Fingers_5() const { return ___Fingers_5; }
	inline List_1_tDA85F67BB9390A7D9940CB6D1B08D2CE04BCE37E ** get_address_of_Fingers_5() { return &___Fingers_5; }
	inline void set_Fingers_5(List_1_tDA85F67BB9390A7D9940CB6D1B08D2CE04BCE37E * value)
	{
		___Fingers_5 = value;
		Il2CppCodeGenWriteBarrier((&___Fingers_5), value);
	}

	inline static int32_t get_offset_of_InactiveFingers_6() { return static_cast<int32_t>(offsetof(LeanTouch_t04E4C85F6ED9E388AD8D7FA10A67E9C74701F15F_StaticFields, ___InactiveFingers_6)); }
	inline List_1_tDA85F67BB9390A7D9940CB6D1B08D2CE04BCE37E * get_InactiveFingers_6() const { return ___InactiveFingers_6; }
	inline List_1_tDA85F67BB9390A7D9940CB6D1B08D2CE04BCE37E ** get_address_of_InactiveFingers_6() { return &___InactiveFingers_6; }
	inline void set_InactiveFingers_6(List_1_tDA85F67BB9390A7D9940CB6D1B08D2CE04BCE37E * value)
	{
		___InactiveFingers_6 = value;
		Il2CppCodeGenWriteBarrier((&___InactiveFingers_6), value);
	}

	inline static int32_t get_offset_of_OnFingerDown_7() { return static_cast<int32_t>(offsetof(LeanTouch_t04E4C85F6ED9E388AD8D7FA10A67E9C74701F15F_StaticFields, ___OnFingerDown_7)); }
	inline Action_1_t955501676F4CE8E1E3A8F8000378BDBB68BCD2EC * get_OnFingerDown_7() const { return ___OnFingerDown_7; }
	inline Action_1_t955501676F4CE8E1E3A8F8000378BDBB68BCD2EC ** get_address_of_OnFingerDown_7() { return &___OnFingerDown_7; }
	inline void set_OnFingerDown_7(Action_1_t955501676F4CE8E1E3A8F8000378BDBB68BCD2EC * value)
	{
		___OnFingerDown_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnFingerDown_7), value);
	}

	inline static int32_t get_offset_of_OnFingerSet_8() { return static_cast<int32_t>(offsetof(LeanTouch_t04E4C85F6ED9E388AD8D7FA10A67E9C74701F15F_StaticFields, ___OnFingerSet_8)); }
	inline Action_1_t955501676F4CE8E1E3A8F8000378BDBB68BCD2EC * get_OnFingerSet_8() const { return ___OnFingerSet_8; }
	inline Action_1_t955501676F4CE8E1E3A8F8000378BDBB68BCD2EC ** get_address_of_OnFingerSet_8() { return &___OnFingerSet_8; }
	inline void set_OnFingerSet_8(Action_1_t955501676F4CE8E1E3A8F8000378BDBB68BCD2EC * value)
	{
		___OnFingerSet_8 = value;
		Il2CppCodeGenWriteBarrier((&___OnFingerSet_8), value);
	}

	inline static int32_t get_offset_of_OnFingerUp_9() { return static_cast<int32_t>(offsetof(LeanTouch_t04E4C85F6ED9E388AD8D7FA10A67E9C74701F15F_StaticFields, ___OnFingerUp_9)); }
	inline Action_1_t955501676F4CE8E1E3A8F8000378BDBB68BCD2EC * get_OnFingerUp_9() const { return ___OnFingerUp_9; }
	inline Action_1_t955501676F4CE8E1E3A8F8000378BDBB68BCD2EC ** get_address_of_OnFingerUp_9() { return &___OnFingerUp_9; }
	inline void set_OnFingerUp_9(Action_1_t955501676F4CE8E1E3A8F8000378BDBB68BCD2EC * value)
	{
		___OnFingerUp_9 = value;
		Il2CppCodeGenWriteBarrier((&___OnFingerUp_9), value);
	}

	inline static int32_t get_offset_of_OnFingerTap_10() { return static_cast<int32_t>(offsetof(LeanTouch_t04E4C85F6ED9E388AD8D7FA10A67E9C74701F15F_StaticFields, ___OnFingerTap_10)); }
	inline Action_1_t955501676F4CE8E1E3A8F8000378BDBB68BCD2EC * get_OnFingerTap_10() const { return ___OnFingerTap_10; }
	inline Action_1_t955501676F4CE8E1E3A8F8000378BDBB68BCD2EC ** get_address_of_OnFingerTap_10() { return &___OnFingerTap_10; }
	inline void set_OnFingerTap_10(Action_1_t955501676F4CE8E1E3A8F8000378BDBB68BCD2EC * value)
	{
		___OnFingerTap_10 = value;
		Il2CppCodeGenWriteBarrier((&___OnFingerTap_10), value);
	}

	inline static int32_t get_offset_of_OnFingerSwipe_11() { return static_cast<int32_t>(offsetof(LeanTouch_t04E4C85F6ED9E388AD8D7FA10A67E9C74701F15F_StaticFields, ___OnFingerSwipe_11)); }
	inline Action_1_t955501676F4CE8E1E3A8F8000378BDBB68BCD2EC * get_OnFingerSwipe_11() const { return ___OnFingerSwipe_11; }
	inline Action_1_t955501676F4CE8E1E3A8F8000378BDBB68BCD2EC ** get_address_of_OnFingerSwipe_11() { return &___OnFingerSwipe_11; }
	inline void set_OnFingerSwipe_11(Action_1_t955501676F4CE8E1E3A8F8000378BDBB68BCD2EC * value)
	{
		___OnFingerSwipe_11 = value;
		Il2CppCodeGenWriteBarrier((&___OnFingerSwipe_11), value);
	}

	inline static int32_t get_offset_of_OnGesture_12() { return static_cast<int32_t>(offsetof(LeanTouch_t04E4C85F6ED9E388AD8D7FA10A67E9C74701F15F_StaticFields, ___OnGesture_12)); }
	inline Action_1_t506E130BAFC89D90111C6710E76354559705EEC2 * get_OnGesture_12() const { return ___OnGesture_12; }
	inline Action_1_t506E130BAFC89D90111C6710E76354559705EEC2 ** get_address_of_OnGesture_12() { return &___OnGesture_12; }
	inline void set_OnGesture_12(Action_1_t506E130BAFC89D90111C6710E76354559705EEC2 * value)
	{
		___OnGesture_12 = value;
		Il2CppCodeGenWriteBarrier((&___OnGesture_12), value);
	}

	inline static int32_t get_offset_of_highestMouseButton_27() { return static_cast<int32_t>(offsetof(LeanTouch_t04E4C85F6ED9E388AD8D7FA10A67E9C74701F15F_StaticFields, ___highestMouseButton_27)); }
	inline int32_t get_highestMouseButton_27() const { return ___highestMouseButton_27; }
	inline int32_t* get_address_of_highestMouseButton_27() { return &___highestMouseButton_27; }
	inline void set_highestMouseButton_27(int32_t value)
	{
		___highestMouseButton_27 = value;
	}

	inline static int32_t get_offset_of_tempRaycastResults_28() { return static_cast<int32_t>(offsetof(LeanTouch_t04E4C85F6ED9E388AD8D7FA10A67E9C74701F15F_StaticFields, ___tempRaycastResults_28)); }
	inline List_1_tB291263EEE72B9F137CA4DC19F039DE672D08028 * get_tempRaycastResults_28() const { return ___tempRaycastResults_28; }
	inline List_1_tB291263EEE72B9F137CA4DC19F039DE672D08028 ** get_address_of_tempRaycastResults_28() { return &___tempRaycastResults_28; }
	inline void set_tempRaycastResults_28(List_1_tB291263EEE72B9F137CA4DC19F039DE672D08028 * value)
	{
		___tempRaycastResults_28 = value;
		Il2CppCodeGenWriteBarrier((&___tempRaycastResults_28), value);
	}

	inline static int32_t get_offset_of_filteredFingers_29() { return static_cast<int32_t>(offsetof(LeanTouch_t04E4C85F6ED9E388AD8D7FA10A67E9C74701F15F_StaticFields, ___filteredFingers_29)); }
	inline List_1_tDA85F67BB9390A7D9940CB6D1B08D2CE04BCE37E * get_filteredFingers_29() const { return ___filteredFingers_29; }
	inline List_1_tDA85F67BB9390A7D9940CB6D1B08D2CE04BCE37E ** get_address_of_filteredFingers_29() { return &___filteredFingers_29; }
	inline void set_filteredFingers_29(List_1_tDA85F67BB9390A7D9940CB6D1B08D2CE04BCE37E * value)
	{
		___filteredFingers_29 = value;
		Il2CppCodeGenWriteBarrier((&___filteredFingers_29), value);
	}

	inline static int32_t get_offset_of_tempPointerEventData_30() { return static_cast<int32_t>(offsetof(LeanTouch_t04E4C85F6ED9E388AD8D7FA10A67E9C74701F15F_StaticFields, ___tempPointerEventData_30)); }
	inline PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63 * get_tempPointerEventData_30() const { return ___tempPointerEventData_30; }
	inline PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63 ** get_address_of_tempPointerEventData_30() { return &___tempPointerEventData_30; }
	inline void set_tempPointerEventData_30(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63 * value)
	{
		___tempPointerEventData_30 = value;
		Il2CppCodeGenWriteBarrier((&___tempPointerEventData_30), value);
	}

	inline static int32_t get_offset_of_tempEventSystem_31() { return static_cast<int32_t>(offsetof(LeanTouch_t04E4C85F6ED9E388AD8D7FA10A67E9C74701F15F_StaticFields, ___tempEventSystem_31)); }
	inline EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 * get_tempEventSystem_31() const { return ___tempEventSystem_31; }
	inline EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 ** get_address_of_tempEventSystem_31() { return &___tempEventSystem_31; }
	inline void set_tempEventSystem_31(EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 * value)
	{
		___tempEventSystem_31 = value;
		Il2CppCodeGenWriteBarrier((&___tempEventSystem_31), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEANTOUCH_T04E4C85F6ED9E388AD8D7FA10A67E9C74701F15F_H
#ifndef LEANTOUCHEVENTS_T62DAC80E5413A13711D733AF81B58B9FF28907F6_H
#define LEANTOUCHEVENTS_T62DAC80E5413A13711D733AF81B58B9FF28907F6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lean.Touch.LeanTouchEvents
struct  LeanTouchEvents_t62DAC80E5413A13711D733AF81B58B9FF28907F6  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEANTOUCHEVENTS_T62DAC80E5413A13711D733AF81B58B9FF28907F6_H
#ifndef LEANTRANSLATE_TA0BF06A17BEE280F19524545165174ABB3C541B5_H
#define LEANTRANSLATE_TA0BF06A17BEE280F19524545165174ABB3C541B5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lean.Touch.LeanTranslate
struct  LeanTranslate_tA0BF06A17BEE280F19524545165174ABB3C541B5  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean Lean.Touch.LeanTranslate::IgnoreGuiFingers
	bool ___IgnoreGuiFingers_4;
	// System.Int32 Lean.Touch.LeanTranslate::RequiredFingerCount
	int32_t ___RequiredFingerCount_5;
	// Lean.Touch.LeanSelectable Lean.Touch.LeanTranslate::RequiredSelectable
	LeanSelectable_t2376D6100D5CC4EEC7F3BB4A3B0D57810CBDA1D9 * ___RequiredSelectable_6;
	// UnityEngine.Camera Lean.Touch.LeanTranslate::Camera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___Camera_7;

public:
	inline static int32_t get_offset_of_IgnoreGuiFingers_4() { return static_cast<int32_t>(offsetof(LeanTranslate_tA0BF06A17BEE280F19524545165174ABB3C541B5, ___IgnoreGuiFingers_4)); }
	inline bool get_IgnoreGuiFingers_4() const { return ___IgnoreGuiFingers_4; }
	inline bool* get_address_of_IgnoreGuiFingers_4() { return &___IgnoreGuiFingers_4; }
	inline void set_IgnoreGuiFingers_4(bool value)
	{
		___IgnoreGuiFingers_4 = value;
	}

	inline static int32_t get_offset_of_RequiredFingerCount_5() { return static_cast<int32_t>(offsetof(LeanTranslate_tA0BF06A17BEE280F19524545165174ABB3C541B5, ___RequiredFingerCount_5)); }
	inline int32_t get_RequiredFingerCount_5() const { return ___RequiredFingerCount_5; }
	inline int32_t* get_address_of_RequiredFingerCount_5() { return &___RequiredFingerCount_5; }
	inline void set_RequiredFingerCount_5(int32_t value)
	{
		___RequiredFingerCount_5 = value;
	}

	inline static int32_t get_offset_of_RequiredSelectable_6() { return static_cast<int32_t>(offsetof(LeanTranslate_tA0BF06A17BEE280F19524545165174ABB3C541B5, ___RequiredSelectable_6)); }
	inline LeanSelectable_t2376D6100D5CC4EEC7F3BB4A3B0D57810CBDA1D9 * get_RequiredSelectable_6() const { return ___RequiredSelectable_6; }
	inline LeanSelectable_t2376D6100D5CC4EEC7F3BB4A3B0D57810CBDA1D9 ** get_address_of_RequiredSelectable_6() { return &___RequiredSelectable_6; }
	inline void set_RequiredSelectable_6(LeanSelectable_t2376D6100D5CC4EEC7F3BB4A3B0D57810CBDA1D9 * value)
	{
		___RequiredSelectable_6 = value;
		Il2CppCodeGenWriteBarrier((&___RequiredSelectable_6), value);
	}

	inline static int32_t get_offset_of_Camera_7() { return static_cast<int32_t>(offsetof(LeanTranslate_tA0BF06A17BEE280F19524545165174ABB3C541B5, ___Camera_7)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_Camera_7() const { return ___Camera_7; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_Camera_7() { return &___Camera_7; }
	inline void set_Camera_7(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___Camera_7 = value;
		Il2CppCodeGenWriteBarrier((&___Camera_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEANTRANSLATE_TA0BF06A17BEE280F19524545165174ABB3C541B5_H
#ifndef LEANTRANSLATESMOOTH_TDEF0850561A46E1A390FA00578EEF60157EA00B4_H
#define LEANTRANSLATESMOOTH_TDEF0850561A46E1A390FA00578EEF60157EA00B4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lean.Touch.LeanTranslateSmooth
struct  LeanTranslateSmooth_tDEF0850561A46E1A390FA00578EEF60157EA00B4  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean Lean.Touch.LeanTranslateSmooth::IgnoreGuiFingers
	bool ___IgnoreGuiFingers_4;
	// System.Int32 Lean.Touch.LeanTranslateSmooth::RequiredFingerCount
	int32_t ___RequiredFingerCount_5;
	// Lean.Touch.LeanSelectable Lean.Touch.LeanTranslateSmooth::RequiredSelectable
	LeanSelectable_t2376D6100D5CC4EEC7F3BB4A3B0D57810CBDA1D9 * ___RequiredSelectable_6;
	// UnityEngine.Camera Lean.Touch.LeanTranslateSmooth::Camera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___Camera_7;
	// System.Single Lean.Touch.LeanTranslateSmooth::Dampening
	float ___Dampening_8;
	// UnityEngine.Vector3 Lean.Touch.LeanTranslateSmooth::RemainingDelta
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___RemainingDelta_9;

public:
	inline static int32_t get_offset_of_IgnoreGuiFingers_4() { return static_cast<int32_t>(offsetof(LeanTranslateSmooth_tDEF0850561A46E1A390FA00578EEF60157EA00B4, ___IgnoreGuiFingers_4)); }
	inline bool get_IgnoreGuiFingers_4() const { return ___IgnoreGuiFingers_4; }
	inline bool* get_address_of_IgnoreGuiFingers_4() { return &___IgnoreGuiFingers_4; }
	inline void set_IgnoreGuiFingers_4(bool value)
	{
		___IgnoreGuiFingers_4 = value;
	}

	inline static int32_t get_offset_of_RequiredFingerCount_5() { return static_cast<int32_t>(offsetof(LeanTranslateSmooth_tDEF0850561A46E1A390FA00578EEF60157EA00B4, ___RequiredFingerCount_5)); }
	inline int32_t get_RequiredFingerCount_5() const { return ___RequiredFingerCount_5; }
	inline int32_t* get_address_of_RequiredFingerCount_5() { return &___RequiredFingerCount_5; }
	inline void set_RequiredFingerCount_5(int32_t value)
	{
		___RequiredFingerCount_5 = value;
	}

	inline static int32_t get_offset_of_RequiredSelectable_6() { return static_cast<int32_t>(offsetof(LeanTranslateSmooth_tDEF0850561A46E1A390FA00578EEF60157EA00B4, ___RequiredSelectable_6)); }
	inline LeanSelectable_t2376D6100D5CC4EEC7F3BB4A3B0D57810CBDA1D9 * get_RequiredSelectable_6() const { return ___RequiredSelectable_6; }
	inline LeanSelectable_t2376D6100D5CC4EEC7F3BB4A3B0D57810CBDA1D9 ** get_address_of_RequiredSelectable_6() { return &___RequiredSelectable_6; }
	inline void set_RequiredSelectable_6(LeanSelectable_t2376D6100D5CC4EEC7F3BB4A3B0D57810CBDA1D9 * value)
	{
		___RequiredSelectable_6 = value;
		Il2CppCodeGenWriteBarrier((&___RequiredSelectable_6), value);
	}

	inline static int32_t get_offset_of_Camera_7() { return static_cast<int32_t>(offsetof(LeanTranslateSmooth_tDEF0850561A46E1A390FA00578EEF60157EA00B4, ___Camera_7)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_Camera_7() const { return ___Camera_7; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_Camera_7() { return &___Camera_7; }
	inline void set_Camera_7(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___Camera_7 = value;
		Il2CppCodeGenWriteBarrier((&___Camera_7), value);
	}

	inline static int32_t get_offset_of_Dampening_8() { return static_cast<int32_t>(offsetof(LeanTranslateSmooth_tDEF0850561A46E1A390FA00578EEF60157EA00B4, ___Dampening_8)); }
	inline float get_Dampening_8() const { return ___Dampening_8; }
	inline float* get_address_of_Dampening_8() { return &___Dampening_8; }
	inline void set_Dampening_8(float value)
	{
		___Dampening_8 = value;
	}

	inline static int32_t get_offset_of_RemainingDelta_9() { return static_cast<int32_t>(offsetof(LeanTranslateSmooth_tDEF0850561A46E1A390FA00578EEF60157EA00B4, ___RemainingDelta_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_RemainingDelta_9() const { return ___RemainingDelta_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_RemainingDelta_9() { return &___RemainingDelta_9; }
	inline void set_RemainingDelta_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___RemainingDelta_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEANTRANSLATESMOOTH_TDEF0850561A46E1A390FA00578EEF60157EA00B4_H
#ifndef LEANPITCHYAWSMOOTH_T9F2DFFF1F54BC5B56381AF346488B6CC7048551E_H
#define LEANPITCHYAWSMOOTH_T9F2DFFF1F54BC5B56381AF346488B6CC7048551E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lean.Touch.LeanPitchYawSmooth
struct  LeanPitchYawSmooth_t9F2DFFF1F54BC5B56381AF346488B6CC7048551E  : public LeanPitchYaw_t8626ADD8CAF55496C8F102476E5815B67482004E
{
public:
	// System.Single Lean.Touch.LeanPitchYawSmooth::Dampening
	float ___Dampening_17;
	// System.Single Lean.Touch.LeanPitchYawSmooth::currentPitch
	float ___currentPitch_18;
	// System.Single Lean.Touch.LeanPitchYawSmooth::currentYaw
	float ___currentYaw_19;

public:
	inline static int32_t get_offset_of_Dampening_17() { return static_cast<int32_t>(offsetof(LeanPitchYawSmooth_t9F2DFFF1F54BC5B56381AF346488B6CC7048551E, ___Dampening_17)); }
	inline float get_Dampening_17() const { return ___Dampening_17; }
	inline float* get_address_of_Dampening_17() { return &___Dampening_17; }
	inline void set_Dampening_17(float value)
	{
		___Dampening_17 = value;
	}

	inline static int32_t get_offset_of_currentPitch_18() { return static_cast<int32_t>(offsetof(LeanPitchYawSmooth_t9F2DFFF1F54BC5B56381AF346488B6CC7048551E, ___currentPitch_18)); }
	inline float get_currentPitch_18() const { return ___currentPitch_18; }
	inline float* get_address_of_currentPitch_18() { return &___currentPitch_18; }
	inline void set_currentPitch_18(float value)
	{
		___currentPitch_18 = value;
	}

	inline static int32_t get_offset_of_currentYaw_19() { return static_cast<int32_t>(offsetof(LeanPitchYawSmooth_t9F2DFFF1F54BC5B56381AF346488B6CC7048551E, ___currentYaw_19)); }
	inline float get_currentYaw_19() const { return ___currentYaw_19; }
	inline float* get_address_of_currentYaw_19() { return &___currentYaw_19; }
	inline void set_currentYaw_19(float value)
	{
		___currentYaw_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEANPITCHYAWSMOOTH_T9F2DFFF1F54BC5B56381AF346488B6CC7048551E_H
#ifndef LEANSELECTABLERENDERERCOLOR_T2F80BABCB4007CEED319D51B0A2FD57378B087D2_H
#define LEANSELECTABLERENDERERCOLOR_T2F80BABCB4007CEED319D51B0A2FD57378B087D2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lean.Touch.LeanSelectableRendererColor
struct  LeanSelectableRendererColor_t2F80BABCB4007CEED319D51B0A2FD57378B087D2  : public LeanSelectableBehaviour_t2F39190FE17A2E1DC2DC7772172DA1E14D3BB6FF
{
public:
	// System.Boolean Lean.Touch.LeanSelectableRendererColor::AutoGetDefaultColor
	bool ___AutoGetDefaultColor_5;
	// UnityEngine.Color Lean.Touch.LeanSelectableRendererColor::DefaultColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___DefaultColor_6;
	// UnityEngine.Color Lean.Touch.LeanSelectableRendererColor::SelectedColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___SelectedColor_7;

public:
	inline static int32_t get_offset_of_AutoGetDefaultColor_5() { return static_cast<int32_t>(offsetof(LeanSelectableRendererColor_t2F80BABCB4007CEED319D51B0A2FD57378B087D2, ___AutoGetDefaultColor_5)); }
	inline bool get_AutoGetDefaultColor_5() const { return ___AutoGetDefaultColor_5; }
	inline bool* get_address_of_AutoGetDefaultColor_5() { return &___AutoGetDefaultColor_5; }
	inline void set_AutoGetDefaultColor_5(bool value)
	{
		___AutoGetDefaultColor_5 = value;
	}

	inline static int32_t get_offset_of_DefaultColor_6() { return static_cast<int32_t>(offsetof(LeanSelectableRendererColor_t2F80BABCB4007CEED319D51B0A2FD57378B087D2, ___DefaultColor_6)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_DefaultColor_6() const { return ___DefaultColor_6; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_DefaultColor_6() { return &___DefaultColor_6; }
	inline void set_DefaultColor_6(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___DefaultColor_6 = value;
	}

	inline static int32_t get_offset_of_SelectedColor_7() { return static_cast<int32_t>(offsetof(LeanSelectableRendererColor_t2F80BABCB4007CEED319D51B0A2FD57378B087D2, ___SelectedColor_7)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_SelectedColor_7() const { return ___SelectedColor_7; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_SelectedColor_7() { return &___SelectedColor_7; }
	inline void set_SelectedColor_7(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___SelectedColor_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEANSELECTABLERENDERERCOLOR_T2F80BABCB4007CEED319D51B0A2FD57378B087D2_H
#ifndef LEANSELECTABLESPRITERENDERERCOLOR_T6B71382EB5C1471F6E461069B72193CBEAF0F411_H
#define LEANSELECTABLESPRITERENDERERCOLOR_T6B71382EB5C1471F6E461069B72193CBEAF0F411_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lean.Touch.LeanSelectableSpriteRendererColor
struct  LeanSelectableSpriteRendererColor_t6B71382EB5C1471F6E461069B72193CBEAF0F411  : public LeanSelectableBehaviour_t2F39190FE17A2E1DC2DC7772172DA1E14D3BB6FF
{
public:
	// System.Boolean Lean.Touch.LeanSelectableSpriteRendererColor::AutoGetDefaultColor
	bool ___AutoGetDefaultColor_5;
	// UnityEngine.Color Lean.Touch.LeanSelectableSpriteRendererColor::DefaultColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___DefaultColor_6;
	// UnityEngine.Color Lean.Touch.LeanSelectableSpriteRendererColor::SelectedColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___SelectedColor_7;

public:
	inline static int32_t get_offset_of_AutoGetDefaultColor_5() { return static_cast<int32_t>(offsetof(LeanSelectableSpriteRendererColor_t6B71382EB5C1471F6E461069B72193CBEAF0F411, ___AutoGetDefaultColor_5)); }
	inline bool get_AutoGetDefaultColor_5() const { return ___AutoGetDefaultColor_5; }
	inline bool* get_address_of_AutoGetDefaultColor_5() { return &___AutoGetDefaultColor_5; }
	inline void set_AutoGetDefaultColor_5(bool value)
	{
		___AutoGetDefaultColor_5 = value;
	}

	inline static int32_t get_offset_of_DefaultColor_6() { return static_cast<int32_t>(offsetof(LeanSelectableSpriteRendererColor_t6B71382EB5C1471F6E461069B72193CBEAF0F411, ___DefaultColor_6)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_DefaultColor_6() const { return ___DefaultColor_6; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_DefaultColor_6() { return &___DefaultColor_6; }
	inline void set_DefaultColor_6(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___DefaultColor_6 = value;
	}

	inline static int32_t get_offset_of_SelectedColor_7() { return static_cast<int32_t>(offsetof(LeanSelectableSpriteRendererColor_t6B71382EB5C1471F6E461069B72193CBEAF0F411, ___SelectedColor_7)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_SelectedColor_7() const { return ___SelectedColor_7; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_SelectedColor_7() { return &___SelectedColor_7; }
	inline void set_SelectedColor_7(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___SelectedColor_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEANSELECTABLESPRITERENDERERCOLOR_T6B71382EB5C1471F6E461069B72193CBEAF0F411_H
#ifndef LEANSELECTABLETRANSLATEINERTIA2D_T4DAC3A4D1C12F2B6B5C05B1416D619D4BDEBB91C_H
#define LEANSELECTABLETRANSLATEINERTIA2D_T4DAC3A4D1C12F2B6B5C05B1416D619D4BDEBB91C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lean.Touch.LeanSelectableTranslateInertia2D
struct  LeanSelectableTranslateInertia2D_t4DAC3A4D1C12F2B6B5C05B1416D619D4BDEBB91C  : public LeanSelectableBehaviour_t2F39190FE17A2E1DC2DC7772172DA1E14D3BB6FF
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEANSELECTABLETRANSLATEINERTIA2D_T4DAC3A4D1C12F2B6B5C05B1416D619D4BDEBB91C_H
#ifndef LEANSELECTABLETRANSLATEINERTIA3D_TE661FD8EBB4B2232AD0EE1A91192EDA72C332022_H
#define LEANSELECTABLETRANSLATEINERTIA3D_TE661FD8EBB4B2232AD0EE1A91192EDA72C332022_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lean.Touch.LeanSelectableTranslateInertia3D
struct  LeanSelectableTranslateInertia3D_tE661FD8EBB4B2232AD0EE1A91192EDA72C332022  : public LeanSelectableBehaviour_t2F39190FE17A2E1DC2DC7772172DA1E14D3BB6FF
{
public:
	// UnityEngine.Rigidbody Lean.Touch.LeanSelectableTranslateInertia3D::body
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * ___body_5;

public:
	inline static int32_t get_offset_of_body_5() { return static_cast<int32_t>(offsetof(LeanSelectableTranslateInertia3D_tE661FD8EBB4B2232AD0EE1A91192EDA72C332022, ___body_5)); }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * get_body_5() const { return ___body_5; }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 ** get_address_of_body_5() { return &___body_5; }
	inline void set_body_5(Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * value)
	{
		___body_5 = value;
		Il2CppCodeGenWriteBarrier((&___body_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEANSELECTABLETRANSLATEINERTIA3D_TE661FD8EBB4B2232AD0EE1A91192EDA72C332022_H
#ifndef LEANTAPSELECT_T055CB6FC5E09E508D97EEC6EFC7C2F8E249AFEF1_H
#define LEANTAPSELECT_T055CB6FC5E09E508D97EEC6EFC7C2F8E249AFEF1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lean.Touch.LeanTapSelect
struct  LeanTapSelect_t055CB6FC5E09E508D97EEC6EFC7C2F8E249AFEF1  : public LeanSelect_t02A1E0856DC63195FF3466117DE7D0A64047B60E
{
public:
	// System.Boolean Lean.Touch.LeanTapSelect::IgnoreGuiFingers
	bool ___IgnoreGuiFingers_10;
	// System.Int32 Lean.Touch.LeanTapSelect::RequiredTapCount
	int32_t ___RequiredTapCount_11;
	// System.Int32 Lean.Touch.LeanTapSelect::RequiredTapInterval
	int32_t ___RequiredTapInterval_12;

public:
	inline static int32_t get_offset_of_IgnoreGuiFingers_10() { return static_cast<int32_t>(offsetof(LeanTapSelect_t055CB6FC5E09E508D97EEC6EFC7C2F8E249AFEF1, ___IgnoreGuiFingers_10)); }
	inline bool get_IgnoreGuiFingers_10() const { return ___IgnoreGuiFingers_10; }
	inline bool* get_address_of_IgnoreGuiFingers_10() { return &___IgnoreGuiFingers_10; }
	inline void set_IgnoreGuiFingers_10(bool value)
	{
		___IgnoreGuiFingers_10 = value;
	}

	inline static int32_t get_offset_of_RequiredTapCount_11() { return static_cast<int32_t>(offsetof(LeanTapSelect_t055CB6FC5E09E508D97EEC6EFC7C2F8E249AFEF1, ___RequiredTapCount_11)); }
	inline int32_t get_RequiredTapCount_11() const { return ___RequiredTapCount_11; }
	inline int32_t* get_address_of_RequiredTapCount_11() { return &___RequiredTapCount_11; }
	inline void set_RequiredTapCount_11(int32_t value)
	{
		___RequiredTapCount_11 = value;
	}

	inline static int32_t get_offset_of_RequiredTapInterval_12() { return static_cast<int32_t>(offsetof(LeanTapSelect_t055CB6FC5E09E508D97EEC6EFC7C2F8E249AFEF1, ___RequiredTapInterval_12)); }
	inline int32_t get_RequiredTapInterval_12() const { return ___RequiredTapInterval_12; }
	inline int32_t* get_address_of_RequiredTapInterval_12() { return &___RequiredTapInterval_12; }
	inline void set_RequiredTapInterval_12(int32_t value)
	{
		___RequiredTapInterval_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEANTAPSELECT_T055CB6FC5E09E508D97EEC6EFC7C2F8E249AFEF1_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3100 = { sizeof (LeanFingerEvent_t1F14AA8CA6D25026BA19E57A021BFE1F51D5FF34), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3101 = { sizeof (LeanFingerTrail_t24936637155A64FC6CF0738216FFDFEF10C90D98), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3101[6] = 
{
	LeanFingerTrail_t24936637155A64FC6CF0738216FFDFEF10C90D98::get_offset_of_IgnoreGuiFingers_4(),
	LeanFingerTrail_t24936637155A64FC6CF0738216FFDFEF10C90D98::get_offset_of_Prefab_5(),
	LeanFingerTrail_t24936637155A64FC6CF0738216FFDFEF10C90D98::get_offset_of_Distance_6(),
	LeanFingerTrail_t24936637155A64FC6CF0738216FFDFEF10C90D98::get_offset_of_MaxLines_7(),
	LeanFingerTrail_t24936637155A64FC6CF0738216FFDFEF10C90D98::get_offset_of_OnFingerUp_8(),
	LeanFingerTrail_t24936637155A64FC6CF0738216FFDFEF10C90D98::get_offset_of_links_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3102 = { sizeof (LeanFingerEvent_tD6458AB7B064E1069692CF3A1289471CB83CF0E0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3103 = { sizeof (Link_t683F8BEE786C9CD7E97AC02899AFC24621551AD5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3103[2] = 
{
	Link_t683F8BEE786C9CD7E97AC02899AFC24621551AD5::get_offset_of_Finger_0(),
	Link_t683F8BEE786C9CD7E97AC02899AFC24621551AD5::get_offset_of_Line_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3104 = { sizeof (LeanFingerUp_t97C1EB0B82492855A1854C258410DAADF00222A3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3104[3] = 
{
	LeanFingerUp_t97C1EB0B82492855A1854C258410DAADF00222A3::get_offset_of_IgnoreIfOverGui_4(),
	LeanFingerUp_t97C1EB0B82492855A1854C258410DAADF00222A3::get_offset_of_IgnoreIfStartedOverGui_5(),
	LeanFingerUp_t97C1EB0B82492855A1854C258410DAADF00222A3::get_offset_of_OnFingerUp_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3105 = { sizeof (LeanFingerEvent_tFF0C9F5C453F71CD3D71C86992C9F95902FD37E3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3106 = { sizeof (LeanMultiTap_t02652AE95847C5613DB2B447D5B0386B5DD943B7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3106[7] = 
{
	LeanMultiTap_t02652AE95847C5613DB2B447D5B0386B5DD943B7::get_offset_of_IgnoreGuiFingers_4(),
	LeanMultiTap_t02652AE95847C5613DB2B447D5B0386B5DD943B7::get_offset_of_MultiTap_5(),
	LeanMultiTap_t02652AE95847C5613DB2B447D5B0386B5DD943B7::get_offset_of_MultiTapCount_6(),
	LeanMultiTap_t02652AE95847C5613DB2B447D5B0386B5DD943B7::get_offset_of_HighestFingerCount_7(),
	LeanMultiTap_t02652AE95847C5613DB2B447D5B0386B5DD943B7::get_offset_of_OnMultiTap_8(),
	LeanMultiTap_t02652AE95847C5613DB2B447D5B0386B5DD943B7::get_offset_of_age_9(),
	LeanMultiTap_t02652AE95847C5613DB2B447D5B0386B5DD943B7::get_offset_of_lastFingerCount_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3107 = { sizeof (IntEvent_tBDB887CFEC7CBB3D58FC47454658620CE252DD88), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3108 = { sizeof (LeanMultiTapInfo_t0A9005A97DB044CE4F2BE882CA91356582EC21B4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3108[1] = 
{
	LeanMultiTapInfo_t0A9005A97DB044CE4F2BE882CA91356582EC21B4::get_offset_of_Text_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3109 = { sizeof (LeanOpenUrl_t9162996B214A907B7225A61EF4DCF2D4ACEDAE93), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3110 = { sizeof (LeanPitchYaw_t8626ADD8CAF55496C8F102476E5815B67482004E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3110[13] = 
{
	LeanPitchYaw_t8626ADD8CAF55496C8F102476E5815B67482004E::get_offset_of_IgnoreGuiFingers_4(),
	LeanPitchYaw_t8626ADD8CAF55496C8F102476E5815B67482004E::get_offset_of_RequiredFingerCount_5(),
	LeanPitchYaw_t8626ADD8CAF55496C8F102476E5815B67482004E::get_offset_of_Camera_6(),
	LeanPitchYaw_t8626ADD8CAF55496C8F102476E5815B67482004E::get_offset_of_Pitch_7(),
	LeanPitchYaw_t8626ADD8CAF55496C8F102476E5815B67482004E::get_offset_of_PitchSensitivity_8(),
	LeanPitchYaw_t8626ADD8CAF55496C8F102476E5815B67482004E::get_offset_of_PitchClamp_9(),
	LeanPitchYaw_t8626ADD8CAF55496C8F102476E5815B67482004E::get_offset_of_PitchMin_10(),
	LeanPitchYaw_t8626ADD8CAF55496C8F102476E5815B67482004E::get_offset_of_PitchMax_11(),
	LeanPitchYaw_t8626ADD8CAF55496C8F102476E5815B67482004E::get_offset_of_Yaw_12(),
	LeanPitchYaw_t8626ADD8CAF55496C8F102476E5815B67482004E::get_offset_of_YawSensitivity_13(),
	LeanPitchYaw_t8626ADD8CAF55496C8F102476E5815B67482004E::get_offset_of_YawClamp_14(),
	LeanPitchYaw_t8626ADD8CAF55496C8F102476E5815B67482004E::get_offset_of_YawMin_15(),
	LeanPitchYaw_t8626ADD8CAF55496C8F102476E5815B67482004E::get_offset_of_YawMax_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3111 = { sizeof (LeanPitchYawSmooth_t9F2DFFF1F54BC5B56381AF346488B6CC7048551E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3111[3] = 
{
	LeanPitchYawSmooth_t9F2DFFF1F54BC5B56381AF346488B6CC7048551E::get_offset_of_Dampening_17(),
	LeanPitchYawSmooth_t9F2DFFF1F54BC5B56381AF346488B6CC7048551E::get_offset_of_currentPitch_18(),
	LeanPitchYawSmooth_t9F2DFFF1F54BC5B56381AF346488B6CC7048551E::get_offset_of_currentYaw_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3112 = { sizeof (LeanPressSelect_t1E686A5833E1CEF9D7E186D8C6B30417D2368F40), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3112[6] = 
{
	LeanPressSelect_t1E686A5833E1CEF9D7E186D8C6B30417D2368F40::get_offset_of_IgnoreGuiFingers_4(),
	LeanPressSelect_t1E686A5833E1CEF9D7E186D8C6B30417D2368F40::get_offset_of_DeselectOnExit_5(),
	LeanPressSelect_t1E686A5833E1CEF9D7E186D8C6B30417D2368F40::get_offset_of_SelectUsing_6(),
	LeanPressSelect_t1E686A5833E1CEF9D7E186D8C6B30417D2368F40::get_offset_of_LayerMask_7(),
	LeanPressSelect_t1E686A5833E1CEF9D7E186D8C6B30417D2368F40::get_offset_of_Search_8(),
	LeanPressSelect_t1E686A5833E1CEF9D7E186D8C6B30417D2368F40::get_offset_of_CurrentSelectables_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3113 = { sizeof (SelectType_tE2E0E9D7ABE838F6EE237CDCE9DC8943ED9C8321)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3113[3] = 
{
	SelectType_tE2E0E9D7ABE838F6EE237CDCE9DC8943ED9C8321::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3114 = { sizeof (SearchType_tE639CAB9A46520EB025A32DAC345D3525AA07CB3)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3114[4] = 
{
	SearchType_tE639CAB9A46520EB025A32DAC345D3525AA07CB3::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3115 = { sizeof (LeanRotate_tF9E60051B47B69E60B506E874877A6377012D1A5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3115[6] = 
{
	LeanRotate_tF9E60051B47B69E60B506E874877A6377012D1A5::get_offset_of_IgnoreGuiFingers_4(),
	LeanRotate_tF9E60051B47B69E60B506E874877A6377012D1A5::get_offset_of_RequiredFingerCount_5(),
	LeanRotate_tF9E60051B47B69E60B506E874877A6377012D1A5::get_offset_of_RequiredSelectable_6(),
	LeanRotate_tF9E60051B47B69E60B506E874877A6377012D1A5::get_offset_of_Camera_7(),
	LeanRotate_tF9E60051B47B69E60B506E874877A6377012D1A5::get_offset_of_RotateAxis_8(),
	LeanRotate_tF9E60051B47B69E60B506E874877A6377012D1A5::get_offset_of_Relative_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3116 = { sizeof (LeanSelect_t02A1E0856DC63195FF3466117DE7D0A64047B60E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3116[6] = 
{
	LeanSelect_t02A1E0856DC63195FF3466117DE7D0A64047B60E::get_offset_of_SelectUsing_4(),
	LeanSelect_t02A1E0856DC63195FF3466117DE7D0A64047B60E::get_offset_of_LayerMask_5(),
	LeanSelect_t02A1E0856DC63195FF3466117DE7D0A64047B60E::get_offset_of_Search_6(),
	LeanSelect_t02A1E0856DC63195FF3466117DE7D0A64047B60E::get_offset_of_CurrentSelectable_7(),
	LeanSelect_t02A1E0856DC63195FF3466117DE7D0A64047B60E::get_offset_of_Reselect_8(),
	LeanSelect_t02A1E0856DC63195FF3466117DE7D0A64047B60E::get_offset_of_AutoDeselect_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3117 = { sizeof (SelectType_t77B376C78F823909EF5808C38F09392243A899C7)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3117[3] = 
{
	SelectType_t77B376C78F823909EF5808C38F09392243A899C7::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3118 = { sizeof (SearchType_tCE9BA229D09A933AB8089F0F2CFF2136FA05BD8D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3118[4] = 
{
	SearchType_tCE9BA229D09A933AB8089F0F2CFF2136FA05BD8D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3119 = { sizeof (ReselectType_t773521332897EB0BC6CDB76604DF8C798F115567)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3119[5] = 
{
	ReselectType_t773521332897EB0BC6CDB76604DF8C798F115567::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3120 = { sizeof (LeanSelectableBehaviour_t2F39190FE17A2E1DC2DC7772172DA1E14D3BB6FF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3120[1] = 
{
	LeanSelectableBehaviour_t2F39190FE17A2E1DC2DC7772172DA1E14D3BB6FF::get_offset_of_selectable_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3121 = { sizeof (LeanSelectableRendererColor_t2F80BABCB4007CEED319D51B0A2FD57378B087D2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3121[3] = 
{
	LeanSelectableRendererColor_t2F80BABCB4007CEED319D51B0A2FD57378B087D2::get_offset_of_AutoGetDefaultColor_5(),
	LeanSelectableRendererColor_t2F80BABCB4007CEED319D51B0A2FD57378B087D2::get_offset_of_DefaultColor_6(),
	LeanSelectableRendererColor_t2F80BABCB4007CEED319D51B0A2FD57378B087D2::get_offset_of_SelectedColor_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3122 = { sizeof (LeanSelectableSpriteRendererColor_t6B71382EB5C1471F6E461069B72193CBEAF0F411), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3122[3] = 
{
	LeanSelectableSpriteRendererColor_t6B71382EB5C1471F6E461069B72193CBEAF0F411::get_offset_of_AutoGetDefaultColor_5(),
	LeanSelectableSpriteRendererColor_t6B71382EB5C1471F6E461069B72193CBEAF0F411::get_offset_of_DefaultColor_6(),
	LeanSelectableSpriteRendererColor_t6B71382EB5C1471F6E461069B72193CBEAF0F411::get_offset_of_SelectedColor_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3123 = { sizeof (LeanSelectableTranslateInertia2D_t4DAC3A4D1C12F2B6B5C05B1416D619D4BDEBB91C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3124 = { sizeof (LeanSelectableTranslateInertia3D_tE661FD8EBB4B2232AD0EE1A91192EDA72C332022), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3124[1] = 
{
	LeanSelectableTranslateInertia3D_tE661FD8EBB4B2232AD0EE1A91192EDA72C332022::get_offset_of_body_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3125 = { sizeof (LeanSpawn_t30A01E3156098F1CF384DBA219F45B0D03F023CC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3125[2] = 
{
	LeanSpawn_t30A01E3156098F1CF384DBA219F45B0D03F023CC::get_offset_of_Prefab_4(),
	LeanSpawn_t30A01E3156098F1CF384DBA219F45B0D03F023CC::get_offset_of_Distance_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3126 = { sizeof (LeanSwipeDirection4_t1C1E954410E25902BD918BD511EB38F89E7FF28B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3126[1] = 
{
	LeanSwipeDirection4_t1C1E954410E25902BD918BD511EB38F89E7FF28B::get_offset_of_InfoText_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3127 = { sizeof (LeanSwipeDirection8_t21FC188D5119A3B629E0E47CCC19F9CD9AD5D1C9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3127[1] = 
{
	LeanSwipeDirection8_t21FC188D5119A3B629E0E47CCC19F9CD9AD5D1C9::get_offset_of_InfoText_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3128 = { sizeof (LeanSwipeRigidbody2D_t9ECD41C52094717AEECDAD91E651FD46147655EF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3128[2] = 
{
	LeanSwipeRigidbody2D_t9ECD41C52094717AEECDAD91E651FD46147655EF::get_offset_of_LayerMask_4(),
	LeanSwipeRigidbody2D_t9ECD41C52094717AEECDAD91E651FD46147655EF::get_offset_of_ForceMultiplier_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3129 = { sizeof (LeanSwipeRigidbody2DNoRelease_tF6F8D1327109A4B89259DAC85D3AE3AAC6A81F32), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3129[3] = 
{
	LeanSwipeRigidbody2DNoRelease_tF6F8D1327109A4B89259DAC85D3AE3AAC6A81F32::get_offset_of_LayerMask_4(),
	LeanSwipeRigidbody2DNoRelease_tF6F8D1327109A4B89259DAC85D3AE3AAC6A81F32::get_offset_of_ImpulseForce_5(),
	LeanSwipeRigidbody2DNoRelease_tF6F8D1327109A4B89259DAC85D3AE3AAC6A81F32::get_offset_of_swipingFinger_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3130 = { sizeof (LeanSwipeRigidbody3D_t9635B49848BD5633E8D0BBBC65AD3F3F203846F8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3130[2] = 
{
	LeanSwipeRigidbody3D_t9635B49848BD5633E8D0BBBC65AD3F3F203846F8::get_offset_of_LayerMask_4(),
	LeanSwipeRigidbody3D_t9635B49848BD5633E8D0BBBC65AD3F3F203846F8::get_offset_of_ForceMultiplier_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3131 = { sizeof (LeanSwipeRigidbody3DNoRelease_t7440DD6AD655BABA1D0B20A6BCA2BCF4D327C823), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3131[3] = 
{
	LeanSwipeRigidbody3DNoRelease_t7440DD6AD655BABA1D0B20A6BCA2BCF4D327C823::get_offset_of_LayerMask_4(),
	LeanSwipeRigidbody3DNoRelease_t7440DD6AD655BABA1D0B20A6BCA2BCF4D327C823::get_offset_of_ImpulseForce_5(),
	LeanSwipeRigidbody3DNoRelease_t7440DD6AD655BABA1D0B20A6BCA2BCF4D327C823::get_offset_of_swipingFinger_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3132 = { sizeof (LeanTapSelect_t055CB6FC5E09E508D97EEC6EFC7C2F8E249AFEF1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3132[3] = 
{
	LeanTapSelect_t055CB6FC5E09E508D97EEC6EFC7C2F8E249AFEF1::get_offset_of_IgnoreGuiFingers_10(),
	LeanTapSelect_t055CB6FC5E09E508D97EEC6EFC7C2F8E249AFEF1::get_offset_of_RequiredTapCount_11(),
	LeanTapSelect_t055CB6FC5E09E508D97EEC6EFC7C2F8E249AFEF1::get_offset_of_RequiredTapInterval_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3133 = { sizeof (LeanTouchEvents_t62DAC80E5413A13711D733AF81B58B9FF28907F6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3134 = { sizeof (LeanTranslate_tA0BF06A17BEE280F19524545165174ABB3C541B5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3134[4] = 
{
	LeanTranslate_tA0BF06A17BEE280F19524545165174ABB3C541B5::get_offset_of_IgnoreGuiFingers_4(),
	LeanTranslate_tA0BF06A17BEE280F19524545165174ABB3C541B5::get_offset_of_RequiredFingerCount_5(),
	LeanTranslate_tA0BF06A17BEE280F19524545165174ABB3C541B5::get_offset_of_RequiredSelectable_6(),
	LeanTranslate_tA0BF06A17BEE280F19524545165174ABB3C541B5::get_offset_of_Camera_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3135 = { sizeof (LeanTranslateSmooth_tDEF0850561A46E1A390FA00578EEF60157EA00B4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3135[6] = 
{
	LeanTranslateSmooth_tDEF0850561A46E1A390FA00578EEF60157EA00B4::get_offset_of_IgnoreGuiFingers_4(),
	LeanTranslateSmooth_tDEF0850561A46E1A390FA00578EEF60157EA00B4::get_offset_of_RequiredFingerCount_5(),
	LeanTranslateSmooth_tDEF0850561A46E1A390FA00578EEF60157EA00B4::get_offset_of_RequiredSelectable_6(),
	LeanTranslateSmooth_tDEF0850561A46E1A390FA00578EEF60157EA00B4::get_offset_of_Camera_7(),
	LeanTranslateSmooth_tDEF0850561A46E1A390FA00578EEF60157EA00B4::get_offset_of_Dampening_8(),
	LeanTranslateSmooth_tDEF0850561A46E1A390FA00578EEF60157EA00B4::get_offset_of_RemainingDelta_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3136 = { sizeof (LeanFinger_t14B8CB80FF55DDB97259D004E0F1149542C099BD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3136[12] = 
{
	LeanFinger_t14B8CB80FF55DDB97259D004E0F1149542C099BD::get_offset_of_Index_0(),
	LeanFinger_t14B8CB80FF55DDB97259D004E0F1149542C099BD::get_offset_of_Age_1(),
	LeanFinger_t14B8CB80FF55DDB97259D004E0F1149542C099BD::get_offset_of_Set_2(),
	LeanFinger_t14B8CB80FF55DDB97259D004E0F1149542C099BD::get_offset_of_LastSet_3(),
	LeanFinger_t14B8CB80FF55DDB97259D004E0F1149542C099BD::get_offset_of_Tap_4(),
	LeanFinger_t14B8CB80FF55DDB97259D004E0F1149542C099BD::get_offset_of_TapCount_5(),
	LeanFinger_t14B8CB80FF55DDB97259D004E0F1149542C099BD::get_offset_of_Swipe_6(),
	LeanFinger_t14B8CB80FF55DDB97259D004E0F1149542C099BD::get_offset_of_StartScreenPosition_7(),
	LeanFinger_t14B8CB80FF55DDB97259D004E0F1149542C099BD::get_offset_of_LastScreenPosition_8(),
	LeanFinger_t14B8CB80FF55DDB97259D004E0F1149542C099BD::get_offset_of_ScreenPosition_9(),
	LeanFinger_t14B8CB80FF55DDB97259D004E0F1149542C099BD::get_offset_of_StartedOverGui_10(),
	LeanFinger_t14B8CB80FF55DDB97259D004E0F1149542C099BD::get_offset_of_Snapshots_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3137 = { sizeof (LeanGesture_t836FA998DDAC09DAD15355F59083CCC443A51FD0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3138 = { sizeof (LeanSelectable_t2376D6100D5CC4EEC7F3BB4A3B0D57810CBDA1D9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3138[6] = 
{
	LeanSelectable_t2376D6100D5CC4EEC7F3BB4A3B0D57810CBDA1D9::get_offset_of_HideWithFinger_4(),
	LeanSelectable_t2376D6100D5CC4EEC7F3BB4A3B0D57810CBDA1D9::get_offset_of_SelectingFinger_5(),
	LeanSelectable_t2376D6100D5CC4EEC7F3BB4A3B0D57810CBDA1D9::get_offset_of_OnSelect_6(),
	LeanSelectable_t2376D6100D5CC4EEC7F3BB4A3B0D57810CBDA1D9::get_offset_of_OnSelectUp_7(),
	LeanSelectable_t2376D6100D5CC4EEC7F3BB4A3B0D57810CBDA1D9::get_offset_of_OnDeselect_8(),
	LeanSelectable_t2376D6100D5CC4EEC7F3BB4A3B0D57810CBDA1D9::get_offset_of_isSelected_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3139 = { sizeof (LeanFingerEvent_t1F6EB317C16105591BB549C360BDBE8E67911B5D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3140 = { sizeof (LeanSnapshot_t345B3981EAE95E412F6DEBEE4C2A8EA07385AA7F), -1, sizeof(LeanSnapshot_t345B3981EAE95E412F6DEBEE4C2A8EA07385AA7F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3140[3] = 
{
	LeanSnapshot_t345B3981EAE95E412F6DEBEE4C2A8EA07385AA7F::get_offset_of_Age_0(),
	LeanSnapshot_t345B3981EAE95E412F6DEBEE4C2A8EA07385AA7F::get_offset_of_ScreenPosition_1(),
	LeanSnapshot_t345B3981EAE95E412F6DEBEE4C2A8EA07385AA7F_StaticFields::get_offset_of_InactiveSnapshots_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3141 = { sizeof (LeanTouch_t04E4C85F6ED9E388AD8D7FA10A67E9C74701F15F), -1, sizeof(LeanTouch_t04E4C85F6ED9E388AD8D7FA10A67E9C74701F15F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3141[28] = 
{
	LeanTouch_t04E4C85F6ED9E388AD8D7FA10A67E9C74701F15F_StaticFields::get_offset_of_Instances_4(),
	LeanTouch_t04E4C85F6ED9E388AD8D7FA10A67E9C74701F15F_StaticFields::get_offset_of_Fingers_5(),
	LeanTouch_t04E4C85F6ED9E388AD8D7FA10A67E9C74701F15F_StaticFields::get_offset_of_InactiveFingers_6(),
	LeanTouch_t04E4C85F6ED9E388AD8D7FA10A67E9C74701F15F_StaticFields::get_offset_of_OnFingerDown_7(),
	LeanTouch_t04E4C85F6ED9E388AD8D7FA10A67E9C74701F15F_StaticFields::get_offset_of_OnFingerSet_8(),
	LeanTouch_t04E4C85F6ED9E388AD8D7FA10A67E9C74701F15F_StaticFields::get_offset_of_OnFingerUp_9(),
	LeanTouch_t04E4C85F6ED9E388AD8D7FA10A67E9C74701F15F_StaticFields::get_offset_of_OnFingerTap_10(),
	LeanTouch_t04E4C85F6ED9E388AD8D7FA10A67E9C74701F15F_StaticFields::get_offset_of_OnFingerSwipe_11(),
	LeanTouch_t04E4C85F6ED9E388AD8D7FA10A67E9C74701F15F_StaticFields::get_offset_of_OnGesture_12(),
	LeanTouch_t04E4C85F6ED9E388AD8D7FA10A67E9C74701F15F::get_offset_of_TapThreshold_13(),
	0,
	LeanTouch_t04E4C85F6ED9E388AD8D7FA10A67E9C74701F15F::get_offset_of_SwipeThreshold_15(),
	0,
	LeanTouch_t04E4C85F6ED9E388AD8D7FA10A67E9C74701F15F::get_offset_of_ReferenceDpi_17(),
	0,
	LeanTouch_t04E4C85F6ED9E388AD8D7FA10A67E9C74701F15F::get_offset_of_GuiLayers_19(),
	LeanTouch_t04E4C85F6ED9E388AD8D7FA10A67E9C74701F15F::get_offset_of_RecordFingers_20(),
	LeanTouch_t04E4C85F6ED9E388AD8D7FA10A67E9C74701F15F::get_offset_of_RecordThreshold_21(),
	LeanTouch_t04E4C85F6ED9E388AD8D7FA10A67E9C74701F15F::get_offset_of_RecordLimit_22(),
	LeanTouch_t04E4C85F6ED9E388AD8D7FA10A67E9C74701F15F::get_offset_of_SimulateMultiFingers_23(),
	LeanTouch_t04E4C85F6ED9E388AD8D7FA10A67E9C74701F15F::get_offset_of_PinchTwistKey_24(),
	LeanTouch_t04E4C85F6ED9E388AD8D7FA10A67E9C74701F15F::get_offset_of_MultiDragKey_25(),
	LeanTouch_t04E4C85F6ED9E388AD8D7FA10A67E9C74701F15F::get_offset_of_FingerTexture_26(),
	LeanTouch_t04E4C85F6ED9E388AD8D7FA10A67E9C74701F15F_StaticFields::get_offset_of_highestMouseButton_27(),
	LeanTouch_t04E4C85F6ED9E388AD8D7FA10A67E9C74701F15F_StaticFields::get_offset_of_tempRaycastResults_28(),
	LeanTouch_t04E4C85F6ED9E388AD8D7FA10A67E9C74701F15F_StaticFields::get_offset_of_filteredFingers_29(),
	LeanTouch_t04E4C85F6ED9E388AD8D7FA10A67E9C74701F15F_StaticFields::get_offset_of_tempPointerEventData_30(),
	LeanTouch_t04E4C85F6ED9E388AD8D7FA10A67E9C74701F15F_StaticFields::get_offset_of_tempEventSystem_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3142 = { sizeof (LeanScale_tDB8973F254335EC3C2393B21E572A79BC89C5D7C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3142[9] = 
{
	LeanScale_tDB8973F254335EC3C2393B21E572A79BC89C5D7C::get_offset_of_IgnoreGuiFingers_4(),
	LeanScale_tDB8973F254335EC3C2393B21E572A79BC89C5D7C::get_offset_of_RequiredFingerCount_5(),
	LeanScale_tDB8973F254335EC3C2393B21E572A79BC89C5D7C::get_offset_of_RequiredSelectable_6(),
	LeanScale_tDB8973F254335EC3C2393B21E572A79BC89C5D7C::get_offset_of_WheelSensitivity_7(),
	LeanScale_tDB8973F254335EC3C2393B21E572A79BC89C5D7C::get_offset_of_Camera_8(),
	LeanScale_tDB8973F254335EC3C2393B21E572A79BC89C5D7C::get_offset_of_Relative_9(),
	LeanScale_tDB8973F254335EC3C2393B21E572A79BC89C5D7C::get_offset_of_ScaleClamp_10(),
	LeanScale_tDB8973F254335EC3C2393B21E572A79BC89C5D7C::get_offset_of_ScaleMin_11(),
	LeanScale_tDB8973F254335EC3C2393B21E572A79BC89C5D7C::get_offset_of_ScaleMax_12(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
