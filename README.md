# ART3D

Este proyecto esta diseñado en Unity junto con Vuforia, para la visualización de elementos virtuales por medio de Realidad Aumentada,
la versión que encuentras aquí se encuentra pública para alumnos, profesores y colaboradores del ITESM (Instituto Tecnológico de Estudios Superiores Monterrey).

Esta Aplicación fue desarrollada como una herramienta de recursos en Realidad Aumentada dirigida a alumnos dentro de los cursos en linea,
a continuación se muetsran las licencias y configuraciones basicas para iniciar este repositorio.

La liberación de esta versión tiene la intencion de brindar las herramientas para la edición e implementación de nuevas funcionalidades, mejoras en el modelado y pocisionamiento
de recursos, las aportaciones por parte de la comunidad seran validadas previamente antes de subirlas con la version oficial.

## Unity
Para poder editar y trabajr sobre este proyecto será necesario descargar Unity y activar los componentes de Vuforia dentro de la descargara de complementos.

Puedes desacargar la ultima versión de [Unity](https://unity3d.com/)

## Vuforia
Para poder crear y editar el proyecto es necesario contar una cuenta de desarrollador(developer) dentro de vuforia, con el proposito de agregar una las llaves de licencia
configuración de objetivos.

[Developer Vuforia](developer.vuforia.com)

Si tienes alguna duda de como iniciar un proyecto con AR(Augmented Reality), te sugerimos este post:

[Como iniciar en AR](https://medium.com/@11hce/ra-una-tecnología-emergente-78be33ea4f38)

### Android
PAra hacer purbas en los dispositivos es necesario configurar e instalar las herramientas de Android studio, asi copmo el SDK sobre el cual estará
trabajando la aplicación.

Puedes descargarlo [Android Studio](https://developer.android.com/studio/index.html)

## Para el desarrollo en IOS es necesario lo siguiente:

**Mac OS: Yosemite + **

**Apple Developer**

Para hacer prubeas de dispositivos apple es necesario contar con una cuenta de desarrollador en [Apple Developer](https://developer.apple.com/)
y descaragar las herramientas actualizadas asi como certificados y entorno de desarrollo.

